﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;


namespace Map
{
    class Map
    {
        StreamReader reader = new StreamReader("map.txt");
        SpriteBatch spriteBatch;

        char[,] mapObjects;
        char[] holdingArray;
        string line;
        int count = 0;
        int mapSizeWidth = 0;
        int mapSizeHeight = 0;

        //Math
        //Location:
        //Index of row * 50;
        //Index of col * 50;
        //All blocks expand 50 pxs long.
        //ENEMIES AND PLAYER ARE EXCEPTIONS!

        public Map(SpriteBatch spriteBatch)
        {
            this.spriteBatch = spriteBatch;

            while ((line = reader.ReadLine()) != null)
            {
                holdingArray = line.ToCharArray();

                for(int x = 0; x < holdingArray.Length; x++)
                {
                    mapObjects[count, x] = holdingArray[x];
                }

                count++;
            }
        }

        public void DrawObjects()
        {
            //spriteBatch.Draw();
        }

        public void ObjectArrayCreator()
        {

        }
    }
}
