﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace Camera
{
    /// <summary>
    /// This is the main type for your game.
    /// </summary>
    public class Game1 : Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;

        // Attributes for Player
        Texture2D playerSprite;
        Rectangle playerPos;

        // Constants for sprite of the Player
        const int PLAYER_X = 0;
        const int PLAYER_Y = 0;
        const int PLAYER_WIDTH = 50;
        const int PLAYER_HEIGHT = 50;
        const int PLAYER_CENTER_X = PLAYER_WIDTH / 2;
        const int PLAYER_CENTER_Y = PLAYER_HEIGHT / 2;

        // Constants for the Player input
        const Keys UP = Keys.Up;
        const Keys LEFT = Keys.Left;
        const Keys DOWN = Keys.Down;
        const Keys RIGHT = Keys.Right;
        const Keys JUMP = Keys.Space;
        const Keys SWITCH = Keys.Z;

        // Enum of active states for Player
        enum PlayerState { Climbing, Crouching, Falling, Jumping, Standing };
        PlayerState pState;

        // Enum of controls for Player
        enum PlayerMove { FaceLeft, FaceRight, FaceUp, Jump, WalkLeft, WalkRight}
        PlayerMove pMove;

        // Modifiable values for Player
        int pAirSpeed = 4;
        int pClimbSpeed = 2;
        int pCrouchSpeed = 2;
        int pWalkSpeed = 5;

        // Attributes for Map
        Texture2D map;
        Rectangle mapPos;

        // Constants for dimensions of the map
        const int MAP_X = 1000;
        const int MAP_Y = 1000;
        const int MAP_WIDTH = 1000;
        const int MAP_HEIGHT = 1000;

        // Attributes for Enemies
        Texture2D enemy1Sprite;
        Rectangle enemy1Pos;

        // Enum of active states for Enemies
        enum EnemyState { Attacking, Standing, Walking };
        EnemyState eState;

        // Attributes for frames
        int frame;
        double timePerFrame = 100;
        int numFrames;
        int framesElapsed;

        // Enum for controlling player or camera
        enum ControlState { Camera, Player };
        ControlState cState;

        // Limits to the view of the map
        // ** DO NOT HAVE PLAYER_LOOK_UP EQUAL OR EXCEED PLAYER_BORDER
        const int PLAYER_LOOK_UP = 50;
        const int PLAYER_BORDER = 100;

        // Attributes for camera centering
        int lookUpCount;
        const int LOOK_UP_SPEED = 3;
        const int CENTER_CAMERA_SPEED = 5;
        const int CENTER_PLAYER_SPEED = 1;
        int centerPlayerCountX;
        int centerPlayerCountY;

        // Miscellaneous attributes
        KeyboardState kState;

        // Method for controlling the camera
        public void Camera()
        {
            // Player hits bounding box on the upper border
            if ((playerPos.Y + PLAYER_CENTER_Y) < ((GraphicsDevice.Viewport.Height / 2) - PLAYER_BORDER))
            {
                cState = ControlState.Camera;
            }

            // Player hits bounding box on the left border
            else if ((playerPos.X + PLAYER_CENTER_X) < ((GraphicsDevice.Viewport.Width / 2) - PLAYER_BORDER))
            {
                cState = ControlState.Camera;
            }

            // Player hits bounding box on the bottom border
            else if ((playerPos.Y + PLAYER_CENTER_Y) > ((GraphicsDevice.Viewport.Height / 2) + PLAYER_BORDER))
            {
                cState = ControlState.Camera;
            }

            // Player hits bounding box on the right border
            else if ((playerPos.X + PLAYER_CENTER_X) > ((GraphicsDevice.Viewport.Width / 2) + PLAYER_BORDER))
            {
                cState = ControlState.Camera;
            }

            // Player doesn't move the camera if they are still in the bounding box
            else
            {
                cState = ControlState.Player;
            }

            // Switches the camera back to the player if they leave the bounding box
            if (cState == ControlState.Camera)
            {
                kState = Keyboard.GetState();

                // Player is on the bounding box on the upper border
                if ((playerPos.Y + PLAYER_CENTER_Y) < (GraphicsDevice.Viewport.Height / 2 - PLAYER_BORDER))
                {
                    // Player leaves the bounding box on the upper border
                    if (kState.IsKeyDown(DOWN))
                    {
                        // Switches control back to the player
                        cState = ControlState.Player;
                    }
                }

                // Player is on the bounding box on the left border
                if ((playerPos.X + PLAYER_CENTER_X) < (GraphicsDevice.Viewport.Width / 2 - PLAYER_BORDER))
                {
                    // Player leaves the bounding box on the left border
                    if (kState.IsKeyDown(RIGHT))
                    {
                        // Switches control back to the player
                        cState = ControlState.Player;
                    }
                }

                // Player is on the bounding box on the bottom border
                if ((playerPos.Y + PLAYER_CENTER_Y) > (GraphicsDevice.Viewport.Height / 2 + PLAYER_BORDER))
                {
                    // Player leaves the bounding box on the bottom border
                    if (kState.IsKeyDown(UP))
                    {
                        // Switches control back to the player
                        cState = ControlState.Player;
                    }
                }

                // Player is on the bounding box on the right border
                if ((playerPos.X + PLAYER_CENTER_X) > (GraphicsDevice.Viewport.Width / 2 + PLAYER_BORDER))
                {
                    // Player leaves the bounding box on the right border
                    if (kState.IsKeyDown(LEFT))
                    {
                        // Switches control back to the player
                        cState = ControlState.Player;
                    }
                }
            }
        }

        // Method for centering the player
        public void CenterPlayer()
        {
            // Tests if the player isn't moving
            if (kState.IsKeyUp(UP) && kState.IsKeyUp(LEFT) && kState.IsKeyUp(DOWN) && kState.IsKeyUp(RIGHT))
            {
                // Tests if player is not in the center on the x-axis
                if (playerPos.X != (GraphicsDevice.Viewport.Width / 2))
                {
                    // Gets the difference of the player's position from the middle
                    centerPlayerCountX = playerPos.X - (GraphicsDevice.Viewport.Width / 2);

                    // Difference is negative
                    if (centerPlayerCountX < 0)
                    {
                        centerPlayerCountX += CENTER_PLAYER_SPEED;
                        playerPos = new Rectangle(playerPos.X += CENTER_PLAYER_SPEED, playerPos.Y, PLAYER_WIDTH, PLAYER_HEIGHT);
                        mapPos = new Rectangle(mapPos.X += CENTER_PLAYER_SPEED, mapPos.Y, MAP_WIDTH, MAP_HEIGHT);
                    }

                    // Difference is positive
                    if (centerPlayerCountX > 0)
                    {
                        centerPlayerCountX -= CENTER_PLAYER_SPEED;
                        playerPos = new Rectangle(playerPos.X -= CENTER_PLAYER_SPEED, playerPos.Y, PLAYER_WIDTH, PLAYER_HEIGHT);
                        mapPos = new Rectangle(mapPos.X -= CENTER_PLAYER_SPEED, mapPos.Y, MAP_WIDTH, MAP_HEIGHT);
                    }
                }

                // Disables up and down camera movement if the player is standing
                if (pState != PlayerState.Standing)
                {
                    // Tests if player is not in the center on the y-axis
                    if (playerPos.Y != (GraphicsDevice.Viewport.Height / 2))
                    {
                        // Gets the difference of the player's position from the middle
                        centerPlayerCountY = playerPos.Y - (GraphicsDevice.Viewport.Height / 2);

                        // Difference is negative
                        if (centerPlayerCountY < 0)
                        {
                            centerPlayerCountY += CENTER_PLAYER_SPEED;
                            playerPos = new Rectangle(playerPos.X, playerPos.Y += CENTER_PLAYER_SPEED, PLAYER_WIDTH, PLAYER_HEIGHT);
                            mapPos = new Rectangle(mapPos.X, mapPos.Y += CENTER_PLAYER_SPEED, MAP_WIDTH, MAP_HEIGHT);
                        }

                        // Difference is positive
                        if (centerPlayerCountY > 0)
                        {
                            centerPlayerCountY -= CENTER_PLAYER_SPEED;
                            playerPos = new Rectangle(playerPos.X, playerPos.Y -= CENTER_PLAYER_SPEED, PLAYER_WIDTH, PLAYER_HEIGHT);
                            mapPos = new Rectangle(mapPos.X, mapPos.Y -= CENTER_PLAYER_SPEED, MAP_WIDTH, MAP_HEIGHT);
                        }
                    }
                }
            }
        }

        // Method for controlling the camera when character is on bounding box
        public void ControlCamera()
        {
            kState = Keyboard.GetState();

            // Player doesn't have the up key held
            if (kState.IsKeyUp(UP))
            {
                lookUpCount -= CENTER_CAMERA_SPEED;

                if (lookUpCount > 0)
                {
                    mapPos = new Rectangle(mapPos.X, mapPos.Y -= CENTER_CAMERA_SPEED, MAP_WIDTH, MAP_HEIGHT);
                    playerPos = new Rectangle(playerPos.X, playerPos.Y -= CENTER_CAMERA_SPEED, PLAYER_WIDTH, PLAYER_HEIGHT);
                }
                if (lookUpCount < 0)
                {
                    mapPos = new Rectangle(mapPos.X, mapPos.Y -= CENTER_CAMERA_SPEED + lookUpCount, MAP_WIDTH, MAP_HEIGHT);
                    playerPos = new Rectangle(playerPos.X, playerPos.Y -= CENTER_CAMERA_SPEED + lookUpCount, PLAYER_WIDTH, PLAYER_HEIGHT);
                    lookUpCount = 0;
                }
            }

            // Player inputs the up key
            if (kState.IsKeyDown(UP))
            {
                // Tests for player states
                switch (pState)
                {
                    case PlayerState.Climbing: mapPos = new Rectangle(mapPos.X, mapPos.Y += pClimbSpeed, MAP_WIDTH, MAP_HEIGHT); break;
                    case PlayerState.Crouching: pState = PlayerState.Standing; break;
                    case PlayerState.Falling: pMove = PlayerMove.FaceUp; LookUp(); break;
                    case PlayerState.Jumping: pMove = PlayerMove.FaceUp; LookUp(); break;
                    case PlayerState.Standing: pMove = PlayerMove.FaceUp; LookUp(); break;
                }
            }

            // Player inputs the left key
            if (kState.IsKeyDown(LEFT))
            {
                // Tests for player states
                switch (pState)
                {
                    case PlayerState.Climbing: mapPos = new Rectangle(mapPos.X += pClimbSpeed, mapPos.Y, MAP_WIDTH, MAP_HEIGHT); break;
                    case PlayerState.Crouching: mapPos = new Rectangle(mapPos.X += pCrouchSpeed, mapPos.Y, MAP_WIDTH, MAP_HEIGHT); break;
                    case PlayerState.Falling: mapPos = new Rectangle(mapPos.X += pAirSpeed, mapPos.Y, MAP_WIDTH, MAP_HEIGHT); break;
                    case PlayerState.Jumping: mapPos = new Rectangle(mapPos.X += pAirSpeed, mapPos.Y, MAP_WIDTH, MAP_HEIGHT); break;
                    case PlayerState.Standing: mapPos = new Rectangle(mapPos.X += pWalkSpeed, mapPos.Y, MAP_WIDTH, MAP_HEIGHT); break;
                }
            }

            // Player doesn't have the down key held
            if (kState.IsKeyUp(DOWN))
            {
                if (pState != PlayerState.Climbing)
                {
                    if (pState != PlayerState.Falling)
                    {
                        pState = PlayerState.Standing;
                    }
                }
            }

            // Player inputs the down key
            if (kState.IsKeyDown(DOWN))
            {
                // Tests for player states
                switch (pState)
                {
                    case PlayerState.Climbing: mapPos = new Rectangle(mapPos.X, mapPos.Y -= pClimbSpeed, MAP_WIDTH, MAP_HEIGHT); break;
                    case PlayerState.Crouching: break;
                    case PlayerState.Falling: break;
                    case PlayerState.Jumping: break;
                    case PlayerState.Standing: pState = PlayerState.Crouching; break;
                }
            }

            // Player inputs the right key
            if (kState.IsKeyDown(RIGHT))
            {
                // Tests for player states
                switch (pState)
                {
                    case PlayerState.Climbing: mapPos = new Rectangle(mapPos.X -= pClimbSpeed, mapPos.Y, MAP_WIDTH, MAP_HEIGHT); break;
                    case PlayerState.Crouching: mapPos = new Rectangle(mapPos.X -= pCrouchSpeed, mapPos.Y, MAP_WIDTH, MAP_HEIGHT); break;
                    case PlayerState.Falling: mapPos = new Rectangle(mapPos.X -= pAirSpeed, mapPos.Y, MAP_WIDTH, MAP_HEIGHT); break;
                    case PlayerState.Jumping: mapPos = new Rectangle(mapPos.X -= pAirSpeed, mapPos.Y, MAP_WIDTH, MAP_HEIGHT); break;
                    case PlayerState.Standing: mapPos = new Rectangle(mapPos.X -= pWalkSpeed, mapPos.Y, MAP_WIDTH, MAP_HEIGHT); break;
                }
            }

            // Player inputs the jump key
            if (kState.IsKeyDown(JUMP))
            {
                // Tests for player states
                switch (pState)
                {
                    case PlayerState.Climbing: pState = PlayerState.Falling; break;
                    case PlayerState.Crouching: pState = PlayerState.Jumping; pMove = PlayerMove.Jump; break;
                    case PlayerState.Falling: break;
                    case PlayerState.Jumping: break;
                    case PlayerState.Standing: pState = PlayerState.Jumping; pMove = PlayerMove.Jump; break;
                }
            }
        }

        // Method for controlling the character
        public void ControlPlayer()
        {
            kState = Keyboard.GetState();

            // Player doesn't have the up key held
            if (kState.IsKeyUp(UP))
            {
                lookUpCount -= CENTER_CAMERA_SPEED;

                if (lookUpCount > 0)
                {
                    mapPos = new Rectangle(mapPos.X, mapPos.Y -= CENTER_CAMERA_SPEED, MAP_WIDTH, MAP_HEIGHT);
                    playerPos = new Rectangle(playerPos.X, playerPos.Y -= CENTER_CAMERA_SPEED, PLAYER_WIDTH, PLAYER_HEIGHT);
                }
                if (lookUpCount < 0)
                {
                    mapPos = new Rectangle(mapPos.X, mapPos.Y -= CENTER_CAMERA_SPEED + lookUpCount, MAP_WIDTH, MAP_HEIGHT);
                    playerPos = new Rectangle(playerPos.X, playerPos.Y -= CENTER_CAMERA_SPEED + lookUpCount, PLAYER_WIDTH, PLAYER_HEIGHT);
                    lookUpCount = 0;
                }
            }

            // Player inputs the up key
            if (kState.IsKeyDown(UP))
            {
                // Tests for player states
                switch(pState)
                {
                    case PlayerState.Climbing: playerPos = new Rectangle(playerPos.X, playerPos.Y -= pClimbSpeed, PLAYER_WIDTH, PLAYER_HEIGHT); break;
                    case PlayerState.Crouching: pState = PlayerState.Standing; break;
                    case PlayerState.Falling: pMove = PlayerMove.FaceUp; LookUp(); break;
                    case PlayerState.Jumping: pMove = PlayerMove.FaceUp; LookUp(); break;
                    case PlayerState.Standing: pMove = PlayerMove.FaceUp; LookUp(); break;
                }
            }

            // Player inputs the left key
            if (kState.IsKeyDown(LEFT))
            {
                // Tests for player states
                switch(pState)
                {
                    case PlayerState.Climbing: playerPos = new Rectangle(playerPos.X -= pClimbSpeed, playerPos.Y, PLAYER_WIDTH, PLAYER_HEIGHT); break;
                    case PlayerState.Crouching: playerPos = new Rectangle(playerPos.X -= pCrouchSpeed, playerPos.Y, PLAYER_WIDTH, PLAYER_HEIGHT); break;
                    case PlayerState.Falling: playerPos = new Rectangle(playerPos.X -= pAirSpeed, playerPos.Y, PLAYER_WIDTH, PLAYER_HEIGHT); break;
                    case PlayerState.Jumping: playerPos = new Rectangle(playerPos.X -= pAirSpeed, playerPos.Y, PLAYER_WIDTH, PLAYER_HEIGHT); break;
                    case PlayerState.Standing: playerPos = new Rectangle(playerPos.X -= pWalkSpeed, playerPos.Y, PLAYER_WIDTH, PLAYER_HEIGHT); break;
                }
            }

            // Player doesn't have the down key held
            if (kState.IsKeyUp(DOWN))
            {
                if (pState != PlayerState.Climbing)
                {
                    if (pState != PlayerState.Falling)
                    {
                        pState = PlayerState.Standing;
                    }        
                }
            }

            // Player inputs the down key
            if (kState.IsKeyDown(DOWN))
            {
                // Tests for player states
                switch(pState)
                {
                    case PlayerState.Climbing: playerPos = new Rectangle(playerPos.X, playerPos.Y += pClimbSpeed, PLAYER_WIDTH, PLAYER_HEIGHT); break;
                    case PlayerState.Crouching: break;
                    case PlayerState.Falling: break;
                    case PlayerState.Jumping: break;
                    case PlayerState.Standing: pState = PlayerState.Crouching; break;
                }
            }

            // Player inputs the right key
            if (kState.IsKeyDown(RIGHT))
            {
                // Tests for player states
                switch(pState)
                {
                    case PlayerState.Climbing: playerPos = new Rectangle(playerPos.X += pClimbSpeed, playerPos.Y, PLAYER_WIDTH, PLAYER_HEIGHT); break;
                    case PlayerState.Crouching: playerPos = new Rectangle(playerPos.X += pCrouchSpeed, playerPos.Y, PLAYER_WIDTH, PLAYER_HEIGHT); break;
                    case PlayerState.Falling: playerPos = new Rectangle(playerPos.X += pAirSpeed, playerPos.Y, PLAYER_WIDTH, PLAYER_HEIGHT); break;
                    case PlayerState.Jumping: playerPos = new Rectangle(playerPos.X += pAirSpeed, playerPos.Y, PLAYER_WIDTH, PLAYER_HEIGHT); break;
                    case PlayerState.Standing: playerPos = new Rectangle(playerPos.X += pWalkSpeed, playerPos.Y, PLAYER_WIDTH, PLAYER_HEIGHT); break;
                }
            }

            // Player inputs the jump key
            if (kState.IsKeyDown(JUMP))
            {
                // Tests for player states
                switch(pState)
                {
                    case PlayerState.Climbing: pState = PlayerState.Falling; break;
                    case PlayerState.Crouching: pState = PlayerState.Jumping; pMove = PlayerMove.Jump; break;
                    case PlayerState.Falling: break;
                    case PlayerState.Jumping: break;
                    case PlayerState.Standing: pState = PlayerState.Jumping; pMove = PlayerMove.Jump; break;
                }
            }
        }

        // Method for the player looking up
        public void LookUp()
        {
            if (lookUpCount <= PLAYER_LOOK_UP)
            {
                lookUpCount += LOOK_UP_SPEED;
                mapPos = new Rectangle(mapPos.X, mapPos.Y += LOOK_UP_SPEED, MAP_WIDTH, MAP_HEIGHT);
                playerPos = new Rectangle(playerPos.X, playerPos.Y += LOOK_UP_SPEED, PLAYER_WIDTH, PLAYER_HEIGHT);
            }
        }

        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            // TODO: Add your initialization logic here
            playerPos = new Rectangle((GraphicsDevice.Viewport.Width / 2), (GraphicsDevice.Viewport.Height / 2), PLAYER_WIDTH, PLAYER_HEIGHT);
            mapPos = new Rectangle((GraphicsDevice.Viewport.Width / 2), (GraphicsDevice.Viewport.Height / 2) + PLAYER_HEIGHT, MAP_WIDTH, MAP_HEIGHT);
            cState = ControlState.Player;
            pState = PlayerState.Climbing;
            pMove = PlayerMove.FaceRight;

            
            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);

            // TODO: use this.Content to load your game content here
            playerSprite = Content.Load<Texture2D>("NeutralArcanine");
            map = Content.Load<Texture2D>("sand");
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// game-specific content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
                Exit();

            // TODO: Add your update logic here

            // Camera method is constantly checking where the player is
            Camera();

            // Camera centers onto the player
            CenterPlayer();

            // Player controls either the player in the bounding box or the camera when the player is on the bounding box
            switch(cState)
            {
                case ControlState.Camera: ControlCamera(); break;
                case ControlState.Player: ControlPlayer(); break;
            }

            base.Update(gameTime);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);

            // TODO: Add your drawing code here
            spriteBatch.Begin();

            // Draws player
            spriteBatch.Draw(playerSprite, playerPos, new Rectangle(PLAYER_X, PLAYER_Y, PLAYER_WIDTH, PLAYER_HEIGHT), Color.White);

            // Draws map
            spriteBatch.Draw(map, mapPos, new Rectangle(MAP_X, MAP_Y, MAP_WIDTH, MAP_HEIGHT), Color.White);

            spriteBatch.End();

            base.Draw(gameTime);
        }
    }
}
