﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
/*
 * Author: Jordan Machalek
 * Defines states for changes in player object throughout game
 * Includes WriteLine statements for debuggind purposes
 * States Key:
 * HB - head + body
 * HBL - head + body + legs
 * HBLA - head + body + legs + arms
 */
namespace PlayerStates
{
    class PlayerStates
    {

        //attributes
        public enum PlayerState { Head, HB, HBL, HBLA } //possible states
        private PlayerState currentState;
        private PlayerState previousState;

        //placeholder attributes - used for testing purposes
        private string condition;

        //properties
        public PlayerState CurrentState { get { return currentState; } }
        public PlayerState PreviousState { get { return previousState; } }

        //placeholder properties
        public string Condition { get { return condition; } set { condition = value; } }

        //constructor
        public PlayerStates()
        {
            condition = "Head";
        }

        //changes current gamestate depending on current/satisfied condiitons
        public PlayerState ChangeState()
        {
            if (condition == "Head")
            {
                currentState = PlayerState.Head;
                Console.WriteLine("Player state = " + currentState);
            }
            else if(condition == "HB")
            {
                currentState = PlayerState.HB;
                Console.WriteLine("Player state = " + currentState);
            }
            else if (condition == "HBL")
            {
                currentState = PlayerState.HBL;
                Console.WriteLine("Player state = " + currentState);
            }
            else if (condition == "HBLA")
            {
                currentState = PlayerState.HBLA;
                Console.WriteLine("Player state = " + currentState);
            }

            return currentState;
        }

        ///Begin methods for states
        ///

        public void Head()
        {

        }

        public void HB()
        {

        }

        public void HBL()
        {

        }

        public void HBLA()
        {

        }

    }
}
