Controls:

A - Moves left
D - Moves Right

Spacebar - Jumps

F1 - Debugging display.


FOLDERS:

Detached:

The main game. Where the game exists.

Camera:

David's original camera. Most of it was implemented, but not
important right now.

ExternalToolDevice:

Where the External Tool exists. Has not been implemented yet
with the rest of the game, but will most likely in Milestone 3.

Files made from the External Tool are saved in:

Detached Code\ExternalToolDevice\ExternalToolDevice\bin\Debug