﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
/*
 * Jordan Machalek
 * 
 */
namespace StatesEnv
{
    class Program
    {
        static void Main(string[] args)
        {
            
            GameStates gs = new GameStates();

            //take input for manually changing states
            while (true)
            {
                Console.WriteLine(gs.CurrentState);

                Console.WriteLine("Enter New State - RETURN to quit");
                string input = Console.ReadLine();

                if(input == "RETURN")
                {
                    return;
                }

                gs.Condition = input;

                gs.ChangeState();
            }

        }
    }
}
