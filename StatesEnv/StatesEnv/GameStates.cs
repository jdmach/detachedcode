﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
/*
 * Author: Jordan Machalek
 * Defines possible game states and handles transition between states
 * Includes WriteLine statements for debuggind purposes
 */
namespace StatesEnv
{
    class GameStates
    {

        //attributes
        public enum GameState { MainMenu, Game, Pause, Credits, GameOver, Restart, Options} //possible states
        private GameState currentState;
        private GameState previousState;

        //placeholder attributes - used for testing purposes
        private string condition;

        //properties
        public GameState CurrentState { get { return currentState; } }
        public GameState PreviousState { get { return previousState; } }

        //placeholder properties
        public string Condition { get { return condition; }  set { condition = value; } }

        //constructor
        public GameStates()
        {
            condition = "MainMenu";
        }

        //changes current gamestate depending on current/satisfied condiitons
        public GameState ChangeState()
        {
            if(condition == "MainMenu")
            {
                currentState = GameState.MainMenu;
                Console.WriteLine("Game state = " + currentState);
            }
            else if(condition == "Game")
            {
                currentState = GameState.Game;
                Console.WriteLine("Game state = " + currentState);
            }
            else if (condition == "Pause")
            {
                currentState = GameState.Pause;
                Console.WriteLine("Game state = " + currentState);
            }
            else if (condition == "Credits")
            {
                currentState = GameState.Credits;
                Console.WriteLine("Game state = " + currentState);
            }
            else if (condition == "GameOver")
            {
                currentState = GameState.GameOver;
                Console.WriteLine("Game state = " + currentState);
            }
            else if (condition == "Restart")
            {
                currentState = GameState.Restart;
                Console.WriteLine("Game state = " + currentState);
            }
            else if (condition == "Options")
            {
                currentState = GameState.Options;
                Console.WriteLine("Game state = " + currentState);
            }

            return currentState;
        }

        ///Begin methods for states
        ///

        public void MainMenu()
        {

        }

        public void Game()
        {

        }

        public void Pause()
        {

        }

        public void Credits()
        {

        }

        public void GameOver()
        {

        }

        public void Restart()
        {

        }

        public void Options()
        {

        }

    }
}
