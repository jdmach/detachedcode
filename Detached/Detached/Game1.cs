﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Detached
{
    /// <summary>
    /// This is the main type for your game.
    /// </summary>
    public class Game1 : Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;

        //create placeholder textures
        Texture2D playerTex;
        Texture2D platformTex;
        Texture2D testTex;

        //fonts
        SpriteFont debugFont;

        //create objects
        Player p1;
        Platform plat1;
        Platform pTest;
        Platform pTest2;
        Input input;
        Physics cphys;
        Camera cam;
        GameStates gState;
        static float time;


        //other attributes
        bool debugOn;
        KeyboardState kstate;
        string debugStr;

        List<Platform> pList;

        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            // TODO: Add your initialization logic here

            /// <author>
            /// Nathan Glick
            /// </author>
            #region
            p1 = new Player(GraphicsDevice.Viewport.Width / 2, 150, 72, 38);
            plat1 = new Platform(0, GraphicsDevice.Viewport.Height - 200, 200, 800);
            pTest = new Platform(100, 100, 50, 100);
            pTest2 = new Platform(400, 200, 50, 100);
            cphys = new Physics();
            input = new Input();
            gState = new GameStates();
            cam = new Camera(GraphicsDevice.Viewport.Width, GraphicsDevice.Viewport.Height);
            debugOn = false;

            pList = new List<Platform>();
            pList.Add(pTest);
            pList.Add(pTest2);
            #endregion

            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);

            // TODO: use this.Content to load your game content here

            //load textures
            
            /// <author>
            /// Nathan Glick
            /// </author>
            #region
            playerTex = Content.Load<Texture2D>("phChar");
            platformTex = Content.Load<Texture2D>("phPlat");
            testTex = Content.Load<Texture2D>("phTest");
            debugFont = Content.Load<SpriteFont>("SpriteFont1");
            #endregion
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// game-specific content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
                Exit();
            /// <author>
            /// Nathan Glick
            /// </author>
            #region
            KeyboardState prevkState = kstate;
            kstate = Keyboard.GetState();

            if (gState.Condition == "MainMenu")
            {
                debugStr = "Current GameState: " + gState.Condition;

                if (kstate.IsKeyDown(Keys.Enter) && prevkState.IsKeyUp(Keys.Enter))
                {
                    gState.Condition = "Game";
                }
                
            }
            else if (gState.Condition == "Game")
            {
                int preX = p1.X;
                int preY = p1.Y;
                
                

                // TODO: Add your update logic here
                time = (float)gameTime.ElapsedGameTime.TotalSeconds;

                cphys.Gravity(p1, plat1, input, time);
                #endregion

                /// <author>
                /// David
                /// </author>
                #region
                // Centers player
                cam.CenterPlayer(p1, kstate);
                foreach (Platform platform in pList)
                {
                    cam.CenterPlatforms(p1, platform, kstate);
                }
                // Takes account of the ground
                cam.CenterPlatforms(p1, plat1, kstate);

                // Movement of map
                if (cam.CheckPlayer(p1) != 0)
                {
                    // Movement of platforms
                    foreach (Platform p in pList)
                    {
                        //cam.MoveCamera(p1, p, kstate);

                        // Right border controls
                        if (cam.CheckPlayer(p1) == 1)
                        {
                            // Moves map as player "moves" right
                            if (kstate.IsKeyDown(Keys.D))
                            {
                                p1.X -= 5;
                                pTest.X -= 5;
                                pTest2.X -= 5;
                                plat1.X -= 5;
                            }
                        }
                        // Left border controls
                        if (cam.CheckPlayer(p1) == 2)
                        {
                            // Moves map as player "moves" left
                            if (kstate.IsKeyDown(Keys.A))
                            {
                                p1.X += 5;
                                pTest.X += 5;
                                pTest2.X += 5;
                                plat1.X += 5;
                            }
                        }
                        // Down border controls                                    Temporarily disabled until bugs can be fixed
                        /*if (cam.CheckPlayer(p1) == 3)
                        {
                            int dif = cam.CheckDistance(p1);
                            p1.Y -= dif;
                            pTest.Y -= dif;
                            pTest2.Y -= dif;
                            plat1.Y -= dif;
                        }
                        // Upper border controls
                        if (cam.CheckPlayer(p1) == 4)
                        {
                            int dif = cam.CheckDistance(p1);
                            p1.Y += dif;
                            pTest.Y += dif;
                            pTest2.Y += dif;
                            plat1.Y += dif;
                        }*/
                    }
                }
                #endregion

                /// <author>
                /// Nathan Glick
                /// </author>
                #region
                foreach (Platform p in pList)
                {
                    cphys.Land(p1, p, input);
                }

                foreach (Platform p in pList)
                { 
                    if (cphys.Collision(p1, p, input))
                    {
                        p1.X = preX;
                    }
                }

                /*cphys.Land(p1, pTest, input);
                cphys.Gravity(p1, plat1, input, time);
                if (cphys.Collision(p1, pTest, input))
                {
                    p1.X = preX;
                }*/



                // Debugging code
                debugStr = "Current GameState: " + gState.Condition + "\n\nPlayer Velocity: " + cphys.Velocity + "\nPlayer Position: " + cphys.Position + "\nPlayer X, Player Y: (" + p1.X + "," + p1.Y + ")\n\npList: ";
                foreach(Platform p in pList)
                {
                    debugStr += "(" + p.X + "," + p.Y + ")";
                }
            }

            if (kstate.IsKeyDown(Keys.F1) && prevkState.IsKeyUp(Keys.F1) && !debugOn) { debugOn = true; }
            else if (kstate.IsKeyDown(Keys.F1) && prevkState.IsKeyUp(Keys.F1) && debugOn) { debugOn = false; }
            #endregion


            base.Update(gameTime);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.White);

            // TODO: Add your drawing code here
            /// <author>
            /// Nathan Glick
            /// </author>
            #region
            spriteBatch.Begin();
            // Draws map
            //spriteBatch.Draw(map, mapPos, new Rectangle(MAP_X, MAP_Y, MAP_WIDTH, MAP_HEIGHT), Color.White);
            if (gState.Condition == "MainMenu")
            {
                spriteBatch.DrawString(debugFont, "Detached\nPress ENTER to start!", new Vector2(GraphicsDevice.Viewport.Width / 3, GraphicsDevice.Viewport.Height / 3), Color.Black);
            }
            if (gState.Condition == "Game")
            {
                
                spriteBatch.Draw(playerTex, p1.Pos, Color.White);
                spriteBatch.Draw(testTex, plat1.Pos, Color.White);
                //spriteBatch.Draw(platformTex, pTest.Pos, Color.White);
                foreach (Platform p in pList)
                {
                    spriteBatch.Draw(platformTex, p.Pos, Color.White);
                }
            }

            if (debugOn)
            {
                //spriteBatch.DrawString(debugFont, "Current GameState: " + gState.Condition + "\n\nPlayer Velocity: " + cphys.Velocity + "\nPlayer Position: " + cphys.Position + "\nPlayer X, Player Y: (" + p1.X + "," + p1.Y + ")", new Vector2(0, 0), Color.Green);
                spriteBatch.DrawString(debugFont, debugStr, new Vector2(0, 0), Color.Green);
            }
            spriteBatch.End();
            #endregion

            base.Draw(gameTime);
        }
    }
}
