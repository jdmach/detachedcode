﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

/// <author>
/// David
/// </author>
#region
namespace Detached
{
    class Camera
    {
        // Attributes
        int windowWidth; // Width of the gameplay window
        int windowHeight; // Height of the gameplay window
        //Player p; // Player in the game

        // Constants
        const int CENTER_SPEED = 1;
        const int BOX_SIZE = 150;

        // Constructor
        public Camera(int w, int h)
        {
            windowWidth = w;
            windowHeight = h;
            //this.p = p;
        }

        // Checking player for position on the box
        public int CheckPlayer(Player p)
        {
            // Right border
            if (p.X >= (windowWidth / 2) + BOX_SIZE)
            {
                return 1;
            }
            // Left border
            else if (p.X <= (windowWidth / 2) - BOX_SIZE)
            {
                return 2;
            }
            // Down border
            else if (p.Y >= (windowHeight / 2) + BOX_SIZE)
            {
                return 3;
            }
            // Up border
            else if (p.Y <= (windowHeight / 2) - BOX_SIZE)
            {
                return 4;
            }
            else
            {
                return 0;
            }
        }

        // Checks the distance the player moves on the box
        public int CheckDistance(Player p)
        {
            // Right border
            if (p.X >= (windowWidth / 2) + BOX_SIZE)
            {
                return p.X - ((windowWidth / 2) + BOX_SIZE);
            }
            // Left border
            else if (p.X <= (windowWidth / 2) - BOX_SIZE)
            {
                return p.X + ((windowWidth / 2) - BOX_SIZE);
            }
            // Down border
            else if (p.Y >= (windowHeight / 2) + BOX_SIZE)
            {
                return p.Y - ((windowHeight / 2) + BOX_SIZE);
            }
            // Up border
            else if (p.Y <= (windowHeight / 2) - BOX_SIZE)
            {
                return p.Y + ((windowHeight / 2) - BOX_SIZE);
            }
            else
            {
                return 0;
            }
        }

        // Moves the map around the player
        public void MoveCamera(Player player, Platform platform, KeyboardState kstate)
        {
            // Right border controls
            if (CheckPlayer(player) == 1)
            {
                // Moves map as player "moves" right
                if (kstate.IsKeyDown(Keys.D))
                {
                    player.X -= 5;
                    platform.X -= 5;
                }
            }
            // Left border controls
            if (CheckPlayer(player) == 2)
            {
                // Moves map as player "moves" left
                if (kstate.IsKeyDown(Keys.A))
                {
                    player.X += 5;
                    platform.X += 5;
                }
            }
        }

        // Method for centering the player when they aren't moving
        public void CenterPlayer(Player player, KeyboardState kstate)
        {
            // Player isn't moving
            if(kstate.IsKeyUp(Keys.A) && kstate.IsKeyUp(Keys.D) && kstate.IsKeyUp(Keys.Space))
            {
                // Player is on the right side of the screen
                if(player.X >= (windowWidth / 2) + 1) // + 1 for uncertainty
                {
                    player.X -= CENTER_SPEED;
                }
                // Player is on the left side of the screen
                if(player.X <= (windowWidth / 2) - 1) // - 1 for uncertainty
                {
                    player.X += CENTER_SPEED;
                }
            }
        }

        public void CenterPlatforms(Player player, Platform platform, KeyboardState kstate)
        {
            // Player isn't moving
            if (kstate.IsKeyUp(Keys.A) && kstate.IsKeyUp(Keys.D) && kstate.IsKeyUp(Keys.Space))
            {
                // Player is on the right side of the screen
                if (player.X >= (windowWidth / 2) + 1) // + 1 for uncertainty
                {
                    platform.X -= CENTER_SPEED;
                }
                // Player is on the left side of the screen
                if (player.X <= (windowWidth / 2) - 1) // - 1 for uncertainty
                {
                    platform.X += CENTER_SPEED;
                }
            }
        }
    }
}
#endregion