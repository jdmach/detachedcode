﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
/// <author>
/// Nathan Glick
/// </author>
#region
namespace Detached
{
    class Physics
    {
        Vector2 gravity = new Vector2(0, 1);
        Vector2 position;
        Vector2 velocity;

        public Vector2 Position
        {
            get { return position; }
        }

        public Vector2 Velocity
        {
            get { return velocity; }
        }

        public void Gravity(Character c, Platform plat, Input input, float t)
        {
            if (input.Move(c))
            {
                c.Y -= 5;
                velocity = new Vector2(0,-5);
                //velocity += gravity;
                position += velocity;
                c.Y += (int)position.Y;

            }
            //velocity += gravity * t;
            //position += velocity * t;
            
            if (c.Pos.Bottom < plat.Pos.Top)
            {
                if (c.Pos.Bottom + (int)position.Y >= plat.Pos.Top)
                {
                    c.Y = plat.Pos.Top - c.Pos.Height;
                }
                else
                {
                    c.Y += (int)position.Y;
                    velocity += gravity * t;
                    position += velocity * t;
                    velocity += gravity;
                }
                
            }

            /*velocity += gravity * t;
            position += velocity * t;
            velocity += gravity;*/

            if (c.Pos.Bottom >= plat.Pos.Top)
            {
                c.Y = plat.Pos.Top - c.Pos.Height;
                velocity = new Vector2(0,0);
                position = new Vector2(0, 0);
                input.CanJump = true;
            }
            /*velocity += gravity * t;
            position += velocity * t;
            velocity += gravity;*/
        }

        public static void Move(Character character, Input input, float t)
        {
            
            if (input.Move(character))
            {
                
            }
            
        }

        public void Land(Character c1, Platform p1, Input input)
        {
            if (c1.Pos.Bottom >= p1.Pos.Top - 1 && (c1.X + c1.Pos.Width >= p1.X + 1 && c1.X <= p1.X + p1.Pos.Width - 1) && c1.Pos.Top < p1.Pos.Bottom)
            {
                position = new Vector2(0, 0);
                velocity = new Vector2(0, 0);
                c1.Y = p1.Pos.Top - c1.Pos.Height;
                input.CanJump = true;
            }
        }

        public bool Collision(Character c1, Platform p1, Input input)
        {
            if (c1.Pos.Intersects(p1.Pos))
            {
                //bool check = true;
                /*if (p1.Pos.Bottom + 2 == c1.Pos.Top)
                {
                    position = new Vector2(0, 0);
                    //c1.Y = c1.Y - 5;
                    //check = false;
                }*/
                if (position != new Vector2(0, 0))
                {
                    c1.Y = p1.Pos.Top - c1.Pos.Height - 1;
                }
                
                
                velocity = new Vector2(0, 0);
                position = new Vector2(0, 0);
                input.CanJump = true;
                return true;
            }

            
            return false;
        }



    }
}
#endregion