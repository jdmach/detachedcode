﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

/// <author>
/// Nathan Glick
/// </author>

namespace Detached
{
    class Input
    {
        KeyboardState kbstate = new KeyboardState();
        KeyboardState prevKbState = new KeyboardState();
        Physics phys = new Physics();
        GameTime gtime = new GameTime();
        bool canJump = true;

        public bool CanJump
        {
            set { canJump = value; }
            get { return canJump; }
        }

        public KeyboardState GetInput()
        {
            return Keyboard.GetState();
        }

        public bool Move(Character character)
        {
            prevKbState = kbstate;
            kbstate = GetInput();

            if (kbstate.IsKeyDown(Keys.A))
            {
                character.X -= 5;
            }
            if (kbstate.IsKeyDown(Keys.D))
            {
                character.X += 5;
            }
            if (kbstate.IsKeyDown(Keys.Space) && prevKbState.IsKeyUp(Keys.Space) && canJump)
            {
                //phys.Jump(character, gtime);
                //character.Y -= 100;
                canJump = false;
                return true;
            }
            return false;
        }

        public void Jump(Character character, Platform plat)
        {
            prevKbState = kbstate;
            kbstate = GetInput();

            if (kbstate.IsKeyDown(Keys.W) && prevKbState.IsKeyUp(Keys.W))
            {
                character.Y -= 60;
            }
        }
        
    }
}
