﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

/// <author>
/// Nathan Glick
/// </author>
#region
namespace Detached
{
    class GameObject
    {
        Rectangle pos;
        int x;
        int y;

        //properties
        public int X
        {
            get { return x; }
            set
            {
                x = value;
                pos.X = x;
            }
        }

        public int Y
        {
            get { return y; }
            set
            {
                y = value;
                pos.Y = y;
            }
        }

        public Rectangle Pos
        {
            get { return pos; }
            set { pos = value; }
        }

        public GameObject(int x, int y, int height, int width)
        {
            X = x;
            Y = y;
            pos = new Rectangle(x, y, width, height);
        }







    }
}
#endregion