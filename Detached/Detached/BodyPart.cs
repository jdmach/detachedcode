﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/// <author>
/// Nathan Glick
/// </author>

namespace Detached
{
    class BodyPart:Item
    {
        public BodyPart(int x, int y, int height, int width) : base(x, y, height, width)
        {

        }
    }
}
