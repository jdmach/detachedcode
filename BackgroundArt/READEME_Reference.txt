Each area has its own numerical designation, based upon the order in which it is visited by the player. E.g. 1

APPLIES ONLY TO UNIQUE ELEMENTS; REPEATED ELEMENTS SHARE SAME DESIGNATION:
Sub-designations progress from the bottom left corner of the area's overall map, moving right and increasing by 1. E.g. 1.1 > 1.2

Reaching the edge of an area, the layer of background sections stacks, continuing with the next number from the left edge. There is no sub-designation for each layer. 
E.g 1.4 is right edge, next layer begins at left edge with 1.5, NOT 1.2.1/1.1.2, etc. EXCEPTION: Repeated elements.

Tutorial: 1
Hub: 2
Tomb: 3
Swamp: 4
Mountain: 5
Forest: 6
Anomoly: 7
