﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace CameraFollow
{
    /// <summary>
    /// This is the main type for your game.
    /// </summary>
    public class Game1 : Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;

        Texture2D character;
        Texture2D environment;
        Vector2 characterPos;
        Vector2 environmentPos;
        KeyboardState kState;

        // Modifiable values
        int climbSpeed = 1;
        int moveSpeed = 5;
        int slowSpeed = 1; // Movement speed for crouching and falling

        // Attributes for test states
        bool onGround = true;
        bool onLeftWall = false;
        bool onRightWall = false;
        bool onClimb = false;
        bool falling = false;
        bool crouching = false;
        bool forceCrouch = false;

        // Attributes for limits
        int lookUpVal;
        int LOOK_UP_LIMIT = 50; // Limit for how high the player can look up

        public void ProcessInput()
        {
            kState = Keyboard.GetState();

            // Movement for camera
            if(kState.IsKeyDown(Keys.W) == true)
            {
                // Makes the player not crouch if they are not in a space where they are forced to crouch
                if(forceCrouch == false)
                {
                    crouching = false;
                }

                // Player looks up if on the ground
                // Caps at 50 so the player doesn't continue looking up
                if(onGround == true)
                {
                    if (lookUpVal != LOOK_UP_LIMIT)
                    {
                        environmentPos.Y += 1;
                        characterPos.Y += 1;
                        lookUpVal++;
                    }
                }

                // Player climbs the vine and no limit is necessary
                if(onClimb == true)
                {
                    environmentPos.Y += climbSpeed;
                }
            }

            // Resets the camera when W is let go
            if(kState.IsKeyDown(Keys.W) == false)
            {
                // Smoothly transitions back at a faster speed
                if(lookUpVal != 0)
                {
                    lookUpVal -= 2;
                    if(lookUpVal < 0)
                    {
                        environmentPos.Y -= 2 + lookUpVal;
                        characterPos.Y -= 2 + lookUpVal;
                        lookUpVal = 0;
                    }
                    else
                    {
                        environmentPos.Y -= 2;
                        characterPos.Y -= 2;
                    }
                }
            }

            if(kState.IsKeyDown(Keys.A) == true)
            {
                // Only allows the camera to move if the player is not against a wall on their left side
                if(onLeftWall == false)
                {
                    // Reduced speed for crouching and falling
                    if(crouching == true || falling == true)
                    {
                        environmentPos.X += slowSpeed;
                    }
                    else
                    {
                        if(onGround == true)
                        {
                            environmentPos.X += moveSpeed;
                        }
                    }
                }
            }

            if(kState.IsKeyDown(Keys.S) == true)
            {
                // Sets state to crouching
                crouching = true;
            }

            if(kState.IsKeyDown(Keys.S) == false)
            {
                // Sets state to standing
                crouching = false;
            }

            if(kState.IsKeyDown(Keys.D) == true)
            {
                // Only allows the camera to move if the player is not against a wall on their right side
                if (onRightWall == false)
                {
                    // Reduced speed for crouching and falling
                    if (crouching == true || falling == true)
                    {
                        environmentPos.X -= slowSpeed;
                    }
                    else
                    {
                        if (onGround == true)
                        {
                            environmentPos.X -= moveSpeed;
                        }
                    }
                }
            }
        }

        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            // TODO: Add your initialization logic here

            characterPos = new Vector2(400, 200);
            environmentPos = new Vector2(400, 250);

            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);

            // TODO: use this.Content to load your game content here
            character = Content.Load<Texture2D>("NeutralArcanine");
            environment = Content.Load<Texture2D>("sand");
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// game-specific content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
                Exit();

            // TODO: Add your update logic here
            ProcessInput();

            base.Update(gameTime);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);

            // TODO: Add your drawing code here
            spriteBatch.Begin();
            spriteBatch.Draw(character, characterPos, Color.White);
            spriteBatch.Draw(environment, environmentPos, Color.White);
            spriteBatch.End();

            base.Draw(gameTime);
        }
    }
}
