﻿namespace ExternalToolDevice
{
    partial class externalForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.clearMapButton = new System.Windows.Forms.Button();
            this.submitMapButton = new System.Windows.Forms.Button();
            this.mapDesignLabel = new System.Windows.Forms.Label();
            this.mapText = new System.Windows.Forms.TextBox();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.enemyImageLocation = new System.Windows.Forms.Label();
            this.enemyFileClear = new System.Windows.Forms.Button();
            this.enemyFileSubmit = new System.Windows.Forms.Button();
            this.enemyFileName = new System.Windows.Forms.TextBox();
            this.enemyAttack = new System.Windows.Forms.TextBox();
            this.enemySpeed = new System.Windows.Forms.TextBox();
            this.enemyHealth = new System.Windows.Forms.TextBox();
            this.enemyHeight = new System.Windows.Forms.TextBox();
            this.enemyWidth = new System.Windows.Forms.TextBox();
            this.enemyY = new System.Windows.Forms.TextBox();
            this.enemyX = new System.Windows.Forms.TextBox();
            this.enemyFileNameLabel = new System.Windows.Forms.Label();
            this.enemyAggressionFalse = new System.Windows.Forms.RadioButton();
            this.enemyAggressionTrue = new System.Windows.Forms.RadioButton();
            this.enemyAggressionLabel = new System.Windows.Forms.Label();
            this.enemyClear = new System.Windows.Forms.Button();
            this.enemySubmit = new System.Windows.Forms.Button();
            this.enemyAttackLabel = new System.Windows.Forms.Label();
            this.enemySpeedLabel = new System.Windows.Forms.Label();
            this.enemyHealthLabel = new System.Windows.Forms.Label();
            this.button3 = new System.Windows.Forms.Button();
            this.enemyHeightLabel = new System.Windows.Forms.Label();
            this.enemyWidthLabel = new System.Windows.Forms.Label();
            this.enemySizeLabel = new System.Windows.Forms.Label();
            this.enemyTextureLabel = new System.Windows.Forms.Label();
            this.enemyLabel = new System.Windows.Forms.Label();
            this.enemyYLabel = new System.Windows.Forms.Label();
            this.enemyXLabel = new System.Windows.Forms.Label();
            this.enemyLocation = new System.Windows.Forms.Label();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.playerImageLocation = new System.Windows.Forms.Label();
            this.playerClear = new System.Windows.Forms.Button();
            this.playerSubmit = new System.Windows.Forms.Button();
            this.playerAttack = new System.Windows.Forms.TextBox();
            this.playerSpeed = new System.Windows.Forms.TextBox();
            this.playerHealth = new System.Windows.Forms.TextBox();
            this.playerHeight = new System.Windows.Forms.TextBox();
            this.playerWidth = new System.Windows.Forms.TextBox();
            this.playerY = new System.Windows.Forms.TextBox();
            this.playerX = new System.Windows.Forms.TextBox();
            this.pAttackLabel = new System.Windows.Forms.Label();
            this.pSpeedLabel = new System.Windows.Forms.Label();
            this.pHealthLabel = new System.Windows.Forms.Label();
            this.pBrowserButton = new System.Windows.Forms.Button();
            this.playerHeightLabel = new System.Windows.Forms.Label();
            this.playerWidthLabel = new System.Windows.Forms.Label();
            this.playerSizeLabel = new System.Windows.Forms.Label();
            this.playerTextureLabel = new System.Windows.Forms.Label();
            this.playerLabel = new System.Windows.Forms.Label();
            this.playerYLocation = new System.Windows.Forms.Label();
            this.playerXLocation = new System.Windows.Forms.Label();
            this.playerLocation = new System.Windows.Forms.Label();
            this.tabControl = new System.Windows.Forms.TabControl();
            this.tabPage6 = new System.Windows.Forms.TabPage();
            this.powerUpFileClear = new System.Windows.Forms.Button();
            this.powerUpFileSubmit = new System.Windows.Forms.Button();
            this.powerUpFileName = new System.Windows.Forms.TextBox();
            this.powerUpFileNameLabel = new System.Windows.Forms.Label();
            this.powerUpClearButton = new System.Windows.Forms.Button();
            this.powerUpSubmitButton = new System.Windows.Forms.Button();
            this.powerUpHeight = new System.Windows.Forms.TextBox();
            this.powerUpWidth = new System.Windows.Forms.TextBox();
            this.powerUpYLocation = new System.Windows.Forms.TextBox();
            this.powerUpXLocation = new System.Windows.Forms.TextBox();
            this.powerUpFileLabel = new System.Windows.Forms.Label();
            this.powerUpBrowseButton = new System.Windows.Forms.Button();
            this.powerUpHeightLabel = new System.Windows.Forms.Label();
            this.powerUpWidthLabel = new System.Windows.Forms.Label();
            this.powerUpYLabel = new System.Windows.Forms.Label();
            this.powerUpXLabel = new System.Windows.Forms.Label();
            this.powerUpTextureLabel = new System.Windows.Forms.Label();
            this.powerUpSizeLabel = new System.Windows.Forms.Label();
            this.powerUpLocationLabel = new System.Windows.Forms.Label();
            this.PowerUpsLabel = new System.Windows.Forms.Label();
            this.tabPage5 = new System.Windows.Forms.TabPage();
            this.bodyPartLocationLabel = new System.Windows.Forms.Label();
            this.bodyPartFileClearButton = new System.Windows.Forms.Button();
            this.bodyPartFileSubmitButton = new System.Windows.Forms.Button();
            this.bodyPartFileName = new System.Windows.Forms.TextBox();
            this.bodyPartFileNameLabel = new System.Windows.Forms.Label();
            this.bodyPartClearButton = new System.Windows.Forms.Button();
            this.bodyPartSubmitButton = new System.Windows.Forms.Button();
            this.bodyPartHeight = new System.Windows.Forms.TextBox();
            this.bodyPartWidth = new System.Windows.Forms.TextBox();
            this.bodyPartY = new System.Windows.Forms.TextBox();
            this.bodyPartX = new System.Windows.Forms.TextBox();
            this.bodyPartFileLocationLabel = new System.Windows.Forms.Label();
            this.bodyPartTextureButton = new System.Windows.Forms.Button();
            this.bodyPartHeightLabel = new System.Windows.Forms.Label();
            this.bodyPartWidthLabel = new System.Windows.Forms.Label();
            this.bodyPartYLabel = new System.Windows.Forms.Label();
            this.bodyPartXLabel = new System.Windows.Forms.Label();
            this.bodyPartTextureLabel = new System.Windows.Forms.Label();
            this.bodyPartSizeLabel = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.bodyPartLabel = new System.Windows.Forms.Label();
            this.tabPage4.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabControl.SuspendLayout();
            this.tabPage6.SuspendLayout();
            this.tabPage5.SuspendLayout();
            this.SuspendLayout();
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.clearMapButton);
            this.tabPage4.Controls.Add(this.submitMapButton);
            this.tabPage4.Controls.Add(this.mapDesignLabel);
            this.tabPage4.Controls.Add(this.mapText);
            this.tabPage4.Location = new System.Drawing.Point(4, 22);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Size = new System.Drawing.Size(728, 639);
            this.tabPage4.TabIndex = 3;
            this.tabPage4.Text = " Map";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // clearMapButton
            // 
            this.clearMapButton.Location = new System.Drawing.Point(352, 400);
            this.clearMapButton.Name = "clearMapButton";
            this.clearMapButton.Size = new System.Drawing.Size(75, 23);
            this.clearMapButton.TabIndex = 3;
            this.clearMapButton.Text = "Clear";
            this.clearMapButton.UseVisualStyleBackColor = true;
            this.clearMapButton.Click += new System.EventHandler(this.clearMapbutton_Click);
            // 
            // submitMapButton
            // 
            this.submitMapButton.Location = new System.Drawing.Point(258, 400);
            this.submitMapButton.Name = "submitMapButton";
            this.submitMapButton.Size = new System.Drawing.Size(75, 23);
            this.submitMapButton.TabIndex = 2;
            this.submitMapButton.Text = "Submit";
            this.submitMapButton.UseVisualStyleBackColor = true;
            this.submitMapButton.Click += new System.EventHandler(this.submitMapButton_Click);
            // 
            // mapDesignLabel
            // 
            this.mapDesignLabel.AutoSize = true;
            this.mapDesignLabel.Location = new System.Drawing.Point(9, 13);
            this.mapDesignLabel.Name = "mapDesignLabel";
            this.mapDesignLabel.Size = new System.Drawing.Size(67, 13);
            this.mapDesignLabel.TabIndex = 1;
            this.mapDesignLabel.Text = "Map Design:";
            // 
            // mapText
            // 
            this.mapText.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.mapText.Location = new System.Drawing.Point(9, 32);
            this.mapText.MaxLength = 600000;
            this.mapText.Multiline = true;
            this.mapText.Name = "mapText";
            this.mapText.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.mapText.Size = new System.Drawing.Size(709, 362);
            this.mapText.TabIndex = 0;
            this.mapText.WordWrap = false;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.enemyImageLocation);
            this.tabPage2.Controls.Add(this.enemyFileClear);
            this.tabPage2.Controls.Add(this.enemyFileSubmit);
            this.tabPage2.Controls.Add(this.enemyFileName);
            this.tabPage2.Controls.Add(this.enemyAttack);
            this.tabPage2.Controls.Add(this.enemySpeed);
            this.tabPage2.Controls.Add(this.enemyHealth);
            this.tabPage2.Controls.Add(this.enemyHeight);
            this.tabPage2.Controls.Add(this.enemyWidth);
            this.tabPage2.Controls.Add(this.enemyY);
            this.tabPage2.Controls.Add(this.enemyX);
            this.tabPage2.Controls.Add(this.enemyFileNameLabel);
            this.tabPage2.Controls.Add(this.enemyAggressionFalse);
            this.tabPage2.Controls.Add(this.enemyAggressionTrue);
            this.tabPage2.Controls.Add(this.enemyAggressionLabel);
            this.tabPage2.Controls.Add(this.enemyClear);
            this.tabPage2.Controls.Add(this.enemySubmit);
            this.tabPage2.Controls.Add(this.enemyAttackLabel);
            this.tabPage2.Controls.Add(this.enemySpeedLabel);
            this.tabPage2.Controls.Add(this.enemyHealthLabel);
            this.tabPage2.Controls.Add(this.button3);
            this.tabPage2.Controls.Add(this.enemyHeightLabel);
            this.tabPage2.Controls.Add(this.enemyWidthLabel);
            this.tabPage2.Controls.Add(this.enemySizeLabel);
            this.tabPage2.Controls.Add(this.enemyTextureLabel);
            this.tabPage2.Controls.Add(this.enemyLabel);
            this.tabPage2.Controls.Add(this.enemyYLabel);
            this.tabPage2.Controls.Add(this.enemyXLabel);
            this.tabPage2.Controls.Add(this.enemyLocation);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(728, 639);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Enemies";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // enemyImageLocation
            // 
            this.enemyImageLocation.AutoSize = true;
            this.enemyImageLocation.Location = new System.Drawing.Point(93, 221);
            this.enemyImageLocation.Name = "enemyImageLocation";
            this.enemyImageLocation.Size = new System.Drawing.Size(73, 13);
            this.enemyImageLocation.TabIndex = 49;
            this.enemyImageLocation.Text = "(File Location)";
            // 
            // enemyFileClear
            // 
            this.enemyFileClear.Location = new System.Drawing.Point(463, 84);
            this.enemyFileClear.Name = "enemyFileClear";
            this.enemyFileClear.Size = new System.Drawing.Size(75, 23);
            this.enemyFileClear.TabIndex = 48;
            this.enemyFileClear.Text = "Clear";
            this.enemyFileClear.UseVisualStyleBackColor = true;
            this.enemyFileClear.Click += new System.EventHandler(this.enemyFileClear_Click);
            // 
            // enemyFileSubmit
            // 
            this.enemyFileSubmit.Location = new System.Drawing.Point(355, 84);
            this.enemyFileSubmit.Name = "enemyFileSubmit";
            this.enemyFileSubmit.Size = new System.Drawing.Size(75, 23);
            this.enemyFileSubmit.TabIndex = 47;
            this.enemyFileSubmit.Text = "Submit";
            this.enemyFileSubmit.UseVisualStyleBackColor = true;
            this.enemyFileSubmit.Click += new System.EventHandler(this.enemyFileSubmit_Click);
            // 
            // enemyFileName
            // 
            this.enemyFileName.Location = new System.Drawing.Point(415, 39);
            this.enemyFileName.Name = "enemyFileName";
            this.enemyFileName.Size = new System.Drawing.Size(100, 20);
            this.enemyFileName.TabIndex = 46;
            // 
            // enemyAttack
            // 
            this.enemyAttack.Location = new System.Drawing.Point(91, 364);
            this.enemyAttack.Name = "enemyAttack";
            this.enemyAttack.Size = new System.Drawing.Size(38, 20);
            this.enemyAttack.TabIndex = 39;
            // 
            // enemySpeed
            // 
            this.enemySpeed.Location = new System.Drawing.Point(91, 309);
            this.enemySpeed.Name = "enemySpeed";
            this.enemySpeed.Size = new System.Drawing.Size(39, 20);
            this.enemySpeed.TabIndex = 37;
            // 
            // enemyHealth
            // 
            this.enemyHealth.Location = new System.Drawing.Point(91, 259);
            this.enemyHealth.Name = "enemyHealth";
            this.enemyHealth.Size = new System.Drawing.Size(39, 20);
            this.enemyHealth.TabIndex = 35;
            // 
            // enemyHeight
            // 
            this.enemyHeight.Location = new System.Drawing.Point(170, 140);
            this.enemyHeight.Name = "enemyHeight";
            this.enemyHeight.Size = new System.Drawing.Size(40, 20);
            this.enemyHeight.TabIndex = 32;
            // 
            // enemyWidth
            // 
            this.enemyWidth.Location = new System.Drawing.Point(65, 140);
            this.enemyWidth.Name = "enemyWidth";
            this.enemyWidth.Size = new System.Drawing.Size(40, 20);
            this.enemyWidth.TabIndex = 31;
            // 
            // enemyY
            // 
            this.enemyY.Location = new System.Drawing.Point(111, 61);
            this.enemyY.Name = "enemyY";
            this.enemyY.Size = new System.Drawing.Size(40, 20);
            this.enemyY.TabIndex = 25;
            // 
            // enemyX
            // 
            this.enemyX.Location = new System.Drawing.Point(42, 61);
            this.enemyX.Name = "enemyX";
            this.enemyX.Size = new System.Drawing.Size(40, 20);
            this.enemyX.TabIndex = 24;
            // 
            // enemyFileNameLabel
            // 
            this.enemyFileNameLabel.AutoSize = true;
            this.enemyFileNameLabel.Location = new System.Drawing.Point(352, 42);
            this.enemyFileNameLabel.Name = "enemyFileNameLabel";
            this.enemyFileNameLabel.Size = new System.Drawing.Size(57, 13);
            this.enemyFileNameLabel.TabIndex = 45;
            this.enemyFileNameLabel.Text = "File Name:";
            // 
            // enemyAggressionFalse
            // 
            this.enemyAggressionFalse.AutoSize = true;
            this.enemyAggressionFalse.Location = new System.Drawing.Point(150, 411);
            this.enemyAggressionFalse.Name = "enemyAggressionFalse";
            this.enemyAggressionFalse.Size = new System.Drawing.Size(39, 17);
            this.enemyAggressionFalse.TabIndex = 44;
            this.enemyAggressionFalse.TabStop = true;
            this.enemyAggressionFalse.Text = "No";
            this.enemyAggressionFalse.UseVisualStyleBackColor = true;
            // 
            // enemyAggressionTrue
            // 
            this.enemyAggressionTrue.AutoSize = true;
            this.enemyAggressionTrue.Location = new System.Drawing.Point(91, 411);
            this.enemyAggressionTrue.Name = "enemyAggressionTrue";
            this.enemyAggressionTrue.Size = new System.Drawing.Size(43, 17);
            this.enemyAggressionTrue.TabIndex = 43;
            this.enemyAggressionTrue.TabStop = true;
            this.enemyAggressionTrue.Text = "Yes";
            this.enemyAggressionTrue.UseVisualStyleBackColor = true;
            // 
            // enemyAggressionLabel
            // 
            this.enemyAggressionLabel.AutoSize = true;
            this.enemyAggressionLabel.Location = new System.Drawing.Point(9, 411);
            this.enemyAggressionLabel.Name = "enemyAggressionLabel";
            this.enemyAggressionLabel.Size = new System.Drawing.Size(65, 13);
            this.enemyAggressionLabel.TabIndex = 42;
            this.enemyAggressionLabel.Text = "Aggression: ";
            // 
            // enemyClear
            // 
            this.enemyClear.Location = new System.Drawing.Point(111, 484);
            this.enemyClear.Name = "enemyClear";
            this.enemyClear.Size = new System.Drawing.Size(75, 23);
            this.enemyClear.TabIndex = 41;
            this.enemyClear.Text = "Clear";
            this.enemyClear.UseVisualStyleBackColor = true;
            this.enemyClear.Click += new System.EventHandler(this.enemyClear_Click);
            // 
            // enemySubmit
            // 
            this.enemySubmit.Location = new System.Drawing.Point(12, 484);
            this.enemySubmit.Name = "enemySubmit";
            this.enemySubmit.Size = new System.Drawing.Size(75, 23);
            this.enemySubmit.TabIndex = 40;
            this.enemySubmit.Text = "Submit";
            this.enemySubmit.UseVisualStyleBackColor = true;
            this.enemySubmit.Click += new System.EventHandler(this.enemySubmitButton_Click);
            // 
            // enemyAttackLabel
            // 
            this.enemyAttackLabel.AutoSize = true;
            this.enemyAttackLabel.Location = new System.Drawing.Point(9, 367);
            this.enemyAttackLabel.Name = "enemyAttackLabel";
            this.enemyAttackLabel.Size = new System.Drawing.Size(79, 13);
            this.enemyAttackLabel.TabIndex = 38;
            this.enemyAttackLabel.Text = "Enemy Attack: ";
            // 
            // enemySpeedLabel
            // 
            this.enemySpeedLabel.AutoSize = true;
            this.enemySpeedLabel.Location = new System.Drawing.Point(9, 312);
            this.enemySpeedLabel.Name = "enemySpeedLabel";
            this.enemySpeedLabel.Size = new System.Drawing.Size(79, 13);
            this.enemySpeedLabel.TabIndex = 36;
            this.enemySpeedLabel.Text = "Enemy Speed: ";
            // 
            // enemyHealthLabel
            // 
            this.enemyHealthLabel.AutoSize = true;
            this.enemyHealthLabel.Location = new System.Drawing.Point(9, 262);
            this.enemyHealthLabel.Name = "enemyHealthLabel";
            this.enemyHealthLabel.Size = new System.Drawing.Size(79, 13);
            this.enemyHealthLabel.TabIndex = 34;
            this.enemyHealthLabel.Text = "Enemy Health: ";
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(12, 216);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(75, 23);
            this.button3.TabIndex = 33;
            this.button3.Text = "Browse";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.pBrowserButton_Click);
            // 
            // enemyHeightLabel
            // 
            this.enemyHeightLabel.AutoSize = true;
            this.enemyHeightLabel.Location = new System.Drawing.Point(120, 143);
            this.enemyHeightLabel.Name = "enemyHeightLabel";
            this.enemyHeightLabel.Size = new System.Drawing.Size(44, 13);
            this.enemyHeightLabel.TabIndex = 30;
            this.enemyHeightLabel.Text = "Height: ";
            // 
            // enemyWidthLabel
            // 
            this.enemyWidthLabel.AutoSize = true;
            this.enemyWidthLabel.Location = new System.Drawing.Point(22, 143);
            this.enemyWidthLabel.Name = "enemyWidthLabel";
            this.enemyWidthLabel.Size = new System.Drawing.Size(41, 13);
            this.enemyWidthLabel.TabIndex = 29;
            this.enemyWidthLabel.Text = "Width: ";
            // 
            // enemySizeLabel
            // 
            this.enemySizeLabel.AutoSize = true;
            this.enemySizeLabel.Location = new System.Drawing.Point(9, 124);
            this.enemySizeLabel.Name = "enemySizeLabel";
            this.enemySizeLabel.Size = new System.Drawing.Size(68, 13);
            this.enemySizeLabel.TabIndex = 28;
            this.enemySizeLabel.Text = "Enemy Size: ";
            // 
            // enemyTextureLabel
            // 
            this.enemyTextureLabel.AutoSize = true;
            this.enemyTextureLabel.Location = new System.Drawing.Point(9, 189);
            this.enemyTextureLabel.Name = "enemyTextureLabel";
            this.enemyTextureLabel.Size = new System.Drawing.Size(84, 13);
            this.enemyTextureLabel.TabIndex = 27;
            this.enemyTextureLabel.Text = "Enemy Texture: ";
            // 
            // enemyLabel
            // 
            this.enemyLabel.AutoSize = true;
            this.enemyLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.enemyLabel.Location = new System.Drawing.Point(8, 12);
            this.enemyLabel.Name = "enemyLabel";
            this.enemyLabel.Size = new System.Drawing.Size(58, 20);
            this.enemyLabel.TabIndex = 26;
            this.enemyLabel.Text = "Enemy";
            // 
            // enemyYLabel
            // 
            this.enemyYLabel.AutoSize = true;
            this.enemyYLabel.Location = new System.Drawing.Point(88, 64);
            this.enemyYLabel.Name = "enemyYLabel";
            this.enemyYLabel.Size = new System.Drawing.Size(17, 13);
            this.enemyYLabel.TabIndex = 23;
            this.enemyYLabel.Text = "Y:";
            // 
            // enemyXLabel
            // 
            this.enemyXLabel.AutoSize = true;
            this.enemyXLabel.Location = new System.Drawing.Point(19, 64);
            this.enemyXLabel.Name = "enemyXLabel";
            this.enemyXLabel.Size = new System.Drawing.Size(17, 13);
            this.enemyXLabel.TabIndex = 22;
            this.enemyXLabel.Text = "X:";
            // 
            // enemyLocation
            // 
            this.enemyLocation.AutoSize = true;
            this.enemyLocation.Location = new System.Drawing.Point(9, 42);
            this.enemyLocation.Name = "enemyLocation";
            this.enemyLocation.Size = new System.Drawing.Size(86, 13);
            this.enemyLocation.TabIndex = 21;
            this.enemyLocation.Text = "Enemy Location:";
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.playerImageLocation);
            this.tabPage1.Controls.Add(this.playerClear);
            this.tabPage1.Controls.Add(this.playerSubmit);
            this.tabPage1.Controls.Add(this.playerAttack);
            this.tabPage1.Controls.Add(this.playerSpeed);
            this.tabPage1.Controls.Add(this.playerHealth);
            this.tabPage1.Controls.Add(this.playerHeight);
            this.tabPage1.Controls.Add(this.playerWidth);
            this.tabPage1.Controls.Add(this.playerY);
            this.tabPage1.Controls.Add(this.playerX);
            this.tabPage1.Controls.Add(this.pAttackLabel);
            this.tabPage1.Controls.Add(this.pSpeedLabel);
            this.tabPage1.Controls.Add(this.pHealthLabel);
            this.tabPage1.Controls.Add(this.pBrowserButton);
            this.tabPage1.Controls.Add(this.playerHeightLabel);
            this.tabPage1.Controls.Add(this.playerWidthLabel);
            this.tabPage1.Controls.Add(this.playerSizeLabel);
            this.tabPage1.Controls.Add(this.playerTextureLabel);
            this.tabPage1.Controls.Add(this.playerLabel);
            this.tabPage1.Controls.Add(this.playerYLocation);
            this.tabPage1.Controls.Add(this.playerXLocation);
            this.tabPage1.Controls.Add(this.playerLocation);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(728, 639);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Player";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // playerImageLocation
            // 
            this.playerImageLocation.AutoSize = true;
            this.playerImageLocation.Location = new System.Drawing.Point(93, 221);
            this.playerImageLocation.Name = "playerImageLocation";
            this.playerImageLocation.Size = new System.Drawing.Size(73, 13);
            this.playerImageLocation.TabIndex = 21;
            this.playerImageLocation.Text = "(File Location)";
            // 
            // playerClear
            // 
            this.playerClear.Location = new System.Drawing.Point(111, 484);
            this.playerClear.Name = "playerClear";
            this.playerClear.Size = new System.Drawing.Size(75, 23);
            this.playerClear.TabIndex = 20;
            this.playerClear.Text = "Clear";
            this.playerClear.UseVisualStyleBackColor = true;
            this.playerClear.Click += new System.EventHandler(this.playerClear_Click);
            // 
            // playerSubmit
            // 
            this.playerSubmit.Location = new System.Drawing.Point(12, 484);
            this.playerSubmit.Name = "playerSubmit";
            this.playerSubmit.Size = new System.Drawing.Size(75, 23);
            this.playerSubmit.TabIndex = 19;
            this.playerSubmit.Text = "Submit";
            this.playerSubmit.UseVisualStyleBackColor = true;
            this.playerSubmit.Click += new System.EventHandler(this.playerSubmitButton_Click);
            // 
            // playerAttack
            // 
            this.playerAttack.Location = new System.Drawing.Point(91, 364);
            this.playerAttack.Name = "playerAttack";
            this.playerAttack.Size = new System.Drawing.Size(38, 20);
            this.playerAttack.TabIndex = 18;
            // 
            // playerSpeed
            // 
            this.playerSpeed.Location = new System.Drawing.Point(91, 309);
            this.playerSpeed.Name = "playerSpeed";
            this.playerSpeed.Size = new System.Drawing.Size(39, 20);
            this.playerSpeed.TabIndex = 16;
            // 
            // playerHealth
            // 
            this.playerHealth.Location = new System.Drawing.Point(91, 259);
            this.playerHealth.Name = "playerHealth";
            this.playerHealth.Size = new System.Drawing.Size(39, 20);
            this.playerHealth.TabIndex = 14;
            // 
            // playerHeight
            // 
            this.playerHeight.Location = new System.Drawing.Point(170, 140);
            this.playerHeight.Name = "playerHeight";
            this.playerHeight.Size = new System.Drawing.Size(40, 20);
            this.playerHeight.TabIndex = 11;
            // 
            // playerWidth
            // 
            this.playerWidth.Location = new System.Drawing.Point(65, 140);
            this.playerWidth.Name = "playerWidth";
            this.playerWidth.Size = new System.Drawing.Size(40, 20);
            this.playerWidth.TabIndex = 10;
            // 
            // playerY
            // 
            this.playerY.Location = new System.Drawing.Point(111, 61);
            this.playerY.Name = "playerY";
            this.playerY.Size = new System.Drawing.Size(40, 20);
            this.playerY.TabIndex = 4;
            // 
            // playerX
            // 
            this.playerX.Location = new System.Drawing.Point(42, 61);
            this.playerX.Name = "playerX";
            this.playerX.Size = new System.Drawing.Size(40, 20);
            this.playerX.TabIndex = 3;
            // 
            // pAttackLabel
            // 
            this.pAttackLabel.AutoSize = true;
            this.pAttackLabel.Location = new System.Drawing.Point(9, 367);
            this.pAttackLabel.Name = "pAttackLabel";
            this.pAttackLabel.Size = new System.Drawing.Size(76, 13);
            this.pAttackLabel.TabIndex = 17;
            this.pAttackLabel.Text = "Player Attack: ";
            // 
            // pSpeedLabel
            // 
            this.pSpeedLabel.AutoSize = true;
            this.pSpeedLabel.Location = new System.Drawing.Point(9, 312);
            this.pSpeedLabel.Name = "pSpeedLabel";
            this.pSpeedLabel.Size = new System.Drawing.Size(76, 13);
            this.pSpeedLabel.TabIndex = 15;
            this.pSpeedLabel.Text = "Player Speed: ";
            // 
            // pHealthLabel
            // 
            this.pHealthLabel.AutoSize = true;
            this.pHealthLabel.Location = new System.Drawing.Point(9, 262);
            this.pHealthLabel.Name = "pHealthLabel";
            this.pHealthLabel.Size = new System.Drawing.Size(76, 13);
            this.pHealthLabel.TabIndex = 13;
            this.pHealthLabel.Text = "Player Health: ";
            // 
            // pBrowserButton
            // 
            this.pBrowserButton.Location = new System.Drawing.Point(12, 216);
            this.pBrowserButton.Name = "pBrowserButton";
            this.pBrowserButton.Size = new System.Drawing.Size(75, 23);
            this.pBrowserButton.TabIndex = 12;
            this.pBrowserButton.Text = "Browse";
            this.pBrowserButton.UseVisualStyleBackColor = true;
            this.pBrowserButton.Click += new System.EventHandler(this.pBrowserButton_Click);
            // 
            // playerHeightLabel
            // 
            this.playerHeightLabel.AutoSize = true;
            this.playerHeightLabel.Location = new System.Drawing.Point(120, 143);
            this.playerHeightLabel.Name = "playerHeightLabel";
            this.playerHeightLabel.Size = new System.Drawing.Size(44, 13);
            this.playerHeightLabel.TabIndex = 9;
            this.playerHeightLabel.Text = "Height: ";
            // 
            // playerWidthLabel
            // 
            this.playerWidthLabel.AutoSize = true;
            this.playerWidthLabel.Location = new System.Drawing.Point(22, 143);
            this.playerWidthLabel.Name = "playerWidthLabel";
            this.playerWidthLabel.Size = new System.Drawing.Size(41, 13);
            this.playerWidthLabel.TabIndex = 8;
            this.playerWidthLabel.Text = "Width: ";
            // 
            // playerSizeLabel
            // 
            this.playerSizeLabel.AutoSize = true;
            this.playerSizeLabel.Location = new System.Drawing.Point(9, 124);
            this.playerSizeLabel.Name = "playerSizeLabel";
            this.playerSizeLabel.Size = new System.Drawing.Size(65, 13);
            this.playerSizeLabel.TabIndex = 7;
            this.playerSizeLabel.Text = "Player Size: ";
            // 
            // playerTextureLabel
            // 
            this.playerTextureLabel.AutoSize = true;
            this.playerTextureLabel.Location = new System.Drawing.Point(9, 189);
            this.playerTextureLabel.Name = "playerTextureLabel";
            this.playerTextureLabel.Size = new System.Drawing.Size(81, 13);
            this.playerTextureLabel.TabIndex = 6;
            this.playerTextureLabel.Text = "Player Texture: ";
            // 
            // playerLabel
            // 
            this.playerLabel.AutoSize = true;
            this.playerLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.playerLabel.Location = new System.Drawing.Point(8, 12);
            this.playerLabel.Name = "playerLabel";
            this.playerLabel.Size = new System.Drawing.Size(52, 20);
            this.playerLabel.TabIndex = 5;
            this.playerLabel.Text = "Player";
            // 
            // playerYLocation
            // 
            this.playerYLocation.AutoSize = true;
            this.playerYLocation.Location = new System.Drawing.Point(88, 64);
            this.playerYLocation.Name = "playerYLocation";
            this.playerYLocation.Size = new System.Drawing.Size(17, 13);
            this.playerYLocation.TabIndex = 2;
            this.playerYLocation.Text = "Y:";
            // 
            // playerXLocation
            // 
            this.playerXLocation.AutoSize = true;
            this.playerXLocation.Location = new System.Drawing.Point(19, 64);
            this.playerXLocation.Name = "playerXLocation";
            this.playerXLocation.Size = new System.Drawing.Size(17, 13);
            this.playerXLocation.TabIndex = 1;
            this.playerXLocation.Text = "X:";
            // 
            // playerLocation
            // 
            this.playerLocation.AutoSize = true;
            this.playerLocation.Location = new System.Drawing.Point(9, 42);
            this.playerLocation.Name = "playerLocation";
            this.playerLocation.Size = new System.Drawing.Size(86, 13);
            this.playerLocation.TabIndex = 0;
            this.playerLocation.Text = "Player Location: ";
            // 
            // tabControl
            // 
            this.tabControl.Controls.Add(this.tabPage1);
            this.tabControl.Controls.Add(this.tabPage2);
            this.tabControl.Controls.Add(this.tabPage6);
            this.tabControl.Controls.Add(this.tabPage5);
            this.tabControl.Controls.Add(this.tabPage4);
            this.tabControl.Location = new System.Drawing.Point(0, -1);
            this.tabControl.Name = "tabControl";
            this.tabControl.SelectedIndex = 0;
            this.tabControl.Size = new System.Drawing.Size(736, 665);
            this.tabControl.TabIndex = 0;
            this.tabControl.SelectedIndexChanged += new System.EventHandler(this.tabControl_SelectedIndexChanged);
            // 
            // tabPage6
            // 
            this.tabPage6.BackColor = System.Drawing.Color.White;
            this.tabPage6.Controls.Add(this.powerUpFileClear);
            this.tabPage6.Controls.Add(this.powerUpFileSubmit);
            this.tabPage6.Controls.Add(this.powerUpFileName);
            this.tabPage6.Controls.Add(this.powerUpFileNameLabel);
            this.tabPage6.Controls.Add(this.powerUpClearButton);
            this.tabPage6.Controls.Add(this.powerUpSubmitButton);
            this.tabPage6.Controls.Add(this.powerUpHeight);
            this.tabPage6.Controls.Add(this.powerUpWidth);
            this.tabPage6.Controls.Add(this.powerUpYLocation);
            this.tabPage6.Controls.Add(this.powerUpXLocation);
            this.tabPage6.Controls.Add(this.powerUpFileLabel);
            this.tabPage6.Controls.Add(this.powerUpBrowseButton);
            this.tabPage6.Controls.Add(this.powerUpHeightLabel);
            this.tabPage6.Controls.Add(this.powerUpWidthLabel);
            this.tabPage6.Controls.Add(this.powerUpYLabel);
            this.tabPage6.Controls.Add(this.powerUpXLabel);
            this.tabPage6.Controls.Add(this.powerUpTextureLabel);
            this.tabPage6.Controls.Add(this.powerUpSizeLabel);
            this.tabPage6.Controls.Add(this.powerUpLocationLabel);
            this.tabPage6.Controls.Add(this.PowerUpsLabel);
            this.tabPage6.Location = new System.Drawing.Point(4, 22);
            this.tabPage6.Name = "tabPage6";
            this.tabPage6.Size = new System.Drawing.Size(728, 639);
            this.tabPage6.TabIndex = 0;
            this.tabPage6.Text = "Power Ups";
            // 
            // powerUpFileClear
            // 
            this.powerUpFileClear.Location = new System.Drawing.Point(463, 84);
            this.powerUpFileClear.Name = "powerUpFileClear";
            this.powerUpFileClear.Size = new System.Drawing.Size(75, 23);
            this.powerUpFileClear.TabIndex = 50;
            this.powerUpFileClear.Text = "Clear";
            this.powerUpFileClear.UseVisualStyleBackColor = true;
            this.powerUpFileClear.Click += new System.EventHandler(this.powerUpFileClear_Click);
            // 
            // powerUpFileSubmit
            // 
            this.powerUpFileSubmit.Location = new System.Drawing.Point(355, 84);
            this.powerUpFileSubmit.Name = "powerUpFileSubmit";
            this.powerUpFileSubmit.Size = new System.Drawing.Size(75, 23);
            this.powerUpFileSubmit.TabIndex = 49;
            this.powerUpFileSubmit.Text = "Submit";
            this.powerUpFileSubmit.UseVisualStyleBackColor = true;
            this.powerUpFileSubmit.Click += new System.EventHandler(this.powerUpFileSubmit_Click);
            // 
            // powerUpFileName
            // 
            this.powerUpFileName.Location = new System.Drawing.Point(415, 39);
            this.powerUpFileName.Name = "powerUpFileName";
            this.powerUpFileName.Size = new System.Drawing.Size(100, 20);
            this.powerUpFileName.TabIndex = 47;
            // 
            // powerUpFileNameLabel
            // 
            this.powerUpFileNameLabel.AutoSize = true;
            this.powerUpFileNameLabel.Location = new System.Drawing.Point(352, 42);
            this.powerUpFileNameLabel.Name = "powerUpFileNameLabel";
            this.powerUpFileNameLabel.Size = new System.Drawing.Size(57, 13);
            this.powerUpFileNameLabel.TabIndex = 44;
            this.powerUpFileNameLabel.Text = "File Name:";
            // 
            // powerUpClearButton
            // 
            this.powerUpClearButton.Location = new System.Drawing.Point(111, 484);
            this.powerUpClearButton.Name = "powerUpClearButton";
            this.powerUpClearButton.Size = new System.Drawing.Size(75, 23);
            this.powerUpClearButton.TabIndex = 43;
            this.powerUpClearButton.Text = "Clear";
            this.powerUpClearButton.UseVisualStyleBackColor = true;
            this.powerUpClearButton.Click += new System.EventHandler(this.powerUpClearButton_Click);
            // 
            // powerUpSubmitButton
            // 
            this.powerUpSubmitButton.Location = new System.Drawing.Point(12, 484);
            this.powerUpSubmitButton.Name = "powerUpSubmitButton";
            this.powerUpSubmitButton.Size = new System.Drawing.Size(75, 23);
            this.powerUpSubmitButton.TabIndex = 42;
            this.powerUpSubmitButton.Text = "Submit";
            this.powerUpSubmitButton.UseVisualStyleBackColor = true;
            this.powerUpSubmitButton.Click += new System.EventHandler(this.powerUpSubmitButton_Click);
            // 
            // powerUpHeight
            // 
            this.powerUpHeight.Location = new System.Drawing.Point(167, 140);
            this.powerUpHeight.Name = "powerUpHeight";
            this.powerUpHeight.Size = new System.Drawing.Size(40, 20);
            this.powerUpHeight.TabIndex = 28;
            // 
            // powerUpWidth
            // 
            this.powerUpWidth.Location = new System.Drawing.Point(66, 140);
            this.powerUpWidth.Name = "powerUpWidth";
            this.powerUpWidth.Size = new System.Drawing.Size(40, 20);
            this.powerUpWidth.TabIndex = 27;
            // 
            // powerUpYLocation
            // 
            this.powerUpYLocation.Location = new System.Drawing.Point(111, 61);
            this.powerUpYLocation.Name = "powerUpYLocation";
            this.powerUpYLocation.Size = new System.Drawing.Size(40, 20);
            this.powerUpYLocation.TabIndex = 26;
            // 
            // powerUpXLocation
            // 
            this.powerUpXLocation.Location = new System.Drawing.Point(42, 61);
            this.powerUpXLocation.Name = "powerUpXLocation";
            this.powerUpXLocation.Size = new System.Drawing.Size(40, 20);
            this.powerUpXLocation.TabIndex = 25;
            // 
            // powerUpFileLabel
            // 
            this.powerUpFileLabel.AutoSize = true;
            this.powerUpFileLabel.Location = new System.Drawing.Point(93, 221);
            this.powerUpFileLabel.Name = "powerUpFileLabel";
            this.powerUpFileLabel.Size = new System.Drawing.Size(73, 13);
            this.powerUpFileLabel.TabIndex = 9;
            this.powerUpFileLabel.Text = "(File Location)";
            // 
            // powerUpBrowseButton
            // 
            this.powerUpBrowseButton.Location = new System.Drawing.Point(12, 216);
            this.powerUpBrowseButton.Name = "powerUpBrowseButton";
            this.powerUpBrowseButton.Size = new System.Drawing.Size(75, 23);
            this.powerUpBrowseButton.TabIndex = 8;
            this.powerUpBrowseButton.Text = "Browse";
            this.powerUpBrowseButton.UseVisualStyleBackColor = true;
            this.powerUpBrowseButton.Click += new System.EventHandler(this.pBrowserButton_Click);
            // 
            // powerUpHeightLabel
            // 
            this.powerUpHeightLabel.AutoSize = true;
            this.powerUpHeightLabel.Location = new System.Drawing.Point(120, 143);
            this.powerUpHeightLabel.Name = "powerUpHeightLabel";
            this.powerUpHeightLabel.Size = new System.Drawing.Size(41, 13);
            this.powerUpHeightLabel.TabIndex = 7;
            this.powerUpHeightLabel.Text = "Height:";
            // 
            // powerUpWidthLabel
            // 
            this.powerUpWidthLabel.AutoSize = true;
            this.powerUpWidthLabel.Location = new System.Drawing.Point(22, 143);
            this.powerUpWidthLabel.Name = "powerUpWidthLabel";
            this.powerUpWidthLabel.Size = new System.Drawing.Size(38, 13);
            this.powerUpWidthLabel.TabIndex = 6;
            this.powerUpWidthLabel.Text = "Width:";
            // 
            // powerUpYLabel
            // 
            this.powerUpYLabel.AutoSize = true;
            this.powerUpYLabel.Location = new System.Drawing.Point(88, 64);
            this.powerUpYLabel.Name = "powerUpYLabel";
            this.powerUpYLabel.Size = new System.Drawing.Size(17, 13);
            this.powerUpYLabel.TabIndex = 5;
            this.powerUpYLabel.Text = "Y:";
            // 
            // powerUpXLabel
            // 
            this.powerUpXLabel.AutoSize = true;
            this.powerUpXLabel.Location = new System.Drawing.Point(19, 64);
            this.powerUpXLabel.Name = "powerUpXLabel";
            this.powerUpXLabel.Size = new System.Drawing.Size(17, 13);
            this.powerUpXLabel.TabIndex = 4;
            this.powerUpXLabel.Text = "X:";
            // 
            // powerUpTextureLabel
            // 
            this.powerUpTextureLabel.AutoSize = true;
            this.powerUpTextureLabel.Location = new System.Drawing.Point(9, 189);
            this.powerUpTextureLabel.Name = "powerUpTextureLabel";
            this.powerUpTextureLabel.Size = new System.Drawing.Size(96, 13);
            this.powerUpTextureLabel.TabIndex = 3;
            this.powerUpTextureLabel.Text = "Power Up Texture:";
            // 
            // powerUpSizeLabel
            // 
            this.powerUpSizeLabel.AutoSize = true;
            this.powerUpSizeLabel.Location = new System.Drawing.Point(9, 124);
            this.powerUpSizeLabel.Name = "powerUpSizeLabel";
            this.powerUpSizeLabel.Size = new System.Drawing.Size(80, 13);
            this.powerUpSizeLabel.TabIndex = 2;
            this.powerUpSizeLabel.Text = "Power Up Size:";
            // 
            // powerUpLocationLabel
            // 
            this.powerUpLocationLabel.AutoSize = true;
            this.powerUpLocationLabel.Location = new System.Drawing.Point(9, 42);
            this.powerUpLocationLabel.Name = "powerUpLocationLabel";
            this.powerUpLocationLabel.Size = new System.Drawing.Size(101, 13);
            this.powerUpLocationLabel.TabIndex = 1;
            this.powerUpLocationLabel.Text = "Power Up Location:";
            // 
            // PowerUpsLabel
            // 
            this.PowerUpsLabel.AutoSize = true;
            this.PowerUpsLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PowerUpsLabel.Location = new System.Drawing.Point(8, 12);
            this.PowerUpsLabel.Name = "PowerUpsLabel";
            this.PowerUpsLabel.Size = new System.Drawing.Size(86, 20);
            this.PowerUpsLabel.TabIndex = 0;
            this.PowerUpsLabel.Text = "Power Ups";
            // 
            // tabPage5
            // 
            this.tabPage5.Controls.Add(this.bodyPartLocationLabel);
            this.tabPage5.Controls.Add(this.bodyPartFileClearButton);
            this.tabPage5.Controls.Add(this.bodyPartFileSubmitButton);
            this.tabPage5.Controls.Add(this.bodyPartFileName);
            this.tabPage5.Controls.Add(this.bodyPartFileNameLabel);
            this.tabPage5.Controls.Add(this.bodyPartClearButton);
            this.tabPage5.Controls.Add(this.bodyPartSubmitButton);
            this.tabPage5.Controls.Add(this.bodyPartHeight);
            this.tabPage5.Controls.Add(this.bodyPartWidth);
            this.tabPage5.Controls.Add(this.bodyPartY);
            this.tabPage5.Controls.Add(this.bodyPartX);
            this.tabPage5.Controls.Add(this.bodyPartFileLocationLabel);
            this.tabPage5.Controls.Add(this.bodyPartTextureButton);
            this.tabPage5.Controls.Add(this.bodyPartHeightLabel);
            this.tabPage5.Controls.Add(this.bodyPartWidthLabel);
            this.tabPage5.Controls.Add(this.bodyPartYLabel);
            this.tabPage5.Controls.Add(this.bodyPartXLabel);
            this.tabPage5.Controls.Add(this.bodyPartTextureLabel);
            this.tabPage5.Controls.Add(this.bodyPartSizeLabel);
            this.tabPage5.Controls.Add(this.label9);
            this.tabPage5.Controls.Add(this.bodyPartLabel);
            this.tabPage5.Location = new System.Drawing.Point(4, 22);
            this.tabPage5.Name = "tabPage5";
            this.tabPage5.Size = new System.Drawing.Size(728, 639);
            this.tabPage5.TabIndex = 4;
            this.tabPage5.Text = "Body Parts";
            this.tabPage5.UseVisualStyleBackColor = true;
            // 
            // bodyPartLocationLabel
            // 
            this.bodyPartLocationLabel.AutoSize = true;
            this.bodyPartLocationLabel.Location = new System.Drawing.Point(9, 42);
            this.bodyPartLocationLabel.Name = "bodyPartLocationLabel";
            this.bodyPartLocationLabel.Size = new System.Drawing.Size(100, 13);
            this.bodyPartLocationLabel.TabIndex = 71;
            this.bodyPartLocationLabel.Text = "Body Part Location:";
            // 
            // bodyPartFileClearButton
            // 
            this.bodyPartFileClearButton.Location = new System.Drawing.Point(463, 84);
            this.bodyPartFileClearButton.Name = "bodyPartFileClearButton";
            this.bodyPartFileClearButton.Size = new System.Drawing.Size(75, 23);
            this.bodyPartFileClearButton.TabIndex = 70;
            this.bodyPartFileClearButton.Text = "Clear";
            this.bodyPartFileClearButton.UseVisualStyleBackColor = true;
            this.bodyPartFileClearButton.Click += new System.EventHandler(this.bodyPartFileClearButton_Click);
            // 
            // bodyPartFileSubmitButton
            // 
            this.bodyPartFileSubmitButton.Location = new System.Drawing.Point(355, 84);
            this.bodyPartFileSubmitButton.Name = "bodyPartFileSubmitButton";
            this.bodyPartFileSubmitButton.Size = new System.Drawing.Size(75, 23);
            this.bodyPartFileSubmitButton.TabIndex = 69;
            this.bodyPartFileSubmitButton.Text = "Submit";
            this.bodyPartFileSubmitButton.UseVisualStyleBackColor = true;
            this.bodyPartFileSubmitButton.Click += new System.EventHandler(this.bodyPartFileSubmitButton_Click);
            // 
            // bodyPartFileName
            // 
            this.bodyPartFileName.Location = new System.Drawing.Point(415, 39);
            this.bodyPartFileName.Name = "bodyPartFileName";
            this.bodyPartFileName.Size = new System.Drawing.Size(100, 20);
            this.bodyPartFileName.TabIndex = 68;
            // 
            // bodyPartFileNameLabel
            // 
            this.bodyPartFileNameLabel.AutoSize = true;
            this.bodyPartFileNameLabel.Location = new System.Drawing.Point(352, 42);
            this.bodyPartFileNameLabel.Name = "bodyPartFileNameLabel";
            this.bodyPartFileNameLabel.Size = new System.Drawing.Size(57, 13);
            this.bodyPartFileNameLabel.TabIndex = 67;
            this.bodyPartFileNameLabel.Text = "File Name:";
            // 
            // bodyPartClearButton
            // 
            this.bodyPartClearButton.Location = new System.Drawing.Point(111, 484);
            this.bodyPartClearButton.Name = "bodyPartClearButton";
            this.bodyPartClearButton.Size = new System.Drawing.Size(75, 23);
            this.bodyPartClearButton.TabIndex = 66;
            this.bodyPartClearButton.Text = "Clear";
            this.bodyPartClearButton.UseVisualStyleBackColor = true;
            this.bodyPartClearButton.Click += new System.EventHandler(this.bodyPartClearButton_Click);
            // 
            // bodyPartSubmitButton
            // 
            this.bodyPartSubmitButton.Location = new System.Drawing.Point(12, 484);
            this.bodyPartSubmitButton.Name = "bodyPartSubmitButton";
            this.bodyPartSubmitButton.Size = new System.Drawing.Size(75, 23);
            this.bodyPartSubmitButton.TabIndex = 65;
            this.bodyPartSubmitButton.Text = "Submit";
            this.bodyPartSubmitButton.UseVisualStyleBackColor = true;
            this.bodyPartSubmitButton.Click += new System.EventHandler(this.bodyPartSubmitButton_Click);
            // 
            // bodyPartHeight
            // 
            this.bodyPartHeight.Location = new System.Drawing.Point(167, 140);
            this.bodyPartHeight.Name = "bodyPartHeight";
            this.bodyPartHeight.Size = new System.Drawing.Size(40, 20);
            this.bodyPartHeight.TabIndex = 64;
            // 
            // bodyPartWidth
            // 
            this.bodyPartWidth.Location = new System.Drawing.Point(66, 140);
            this.bodyPartWidth.Name = "bodyPartWidth";
            this.bodyPartWidth.Size = new System.Drawing.Size(40, 20);
            this.bodyPartWidth.TabIndex = 63;
            // 
            // bodyPartY
            // 
            this.bodyPartY.Location = new System.Drawing.Point(111, 61);
            this.bodyPartY.Name = "bodyPartY";
            this.bodyPartY.Size = new System.Drawing.Size(40, 20);
            this.bodyPartY.TabIndex = 62;
            // 
            // bodyPartX
            // 
            this.bodyPartX.Location = new System.Drawing.Point(42, 61);
            this.bodyPartX.Name = "bodyPartX";
            this.bodyPartX.Size = new System.Drawing.Size(40, 20);
            this.bodyPartX.TabIndex = 61;
            // 
            // bodyPartFileLocationLabel
            // 
            this.bodyPartFileLocationLabel.AutoSize = true;
            this.bodyPartFileLocationLabel.Location = new System.Drawing.Point(93, 221);
            this.bodyPartFileLocationLabel.Name = "bodyPartFileLocationLabel";
            this.bodyPartFileLocationLabel.Size = new System.Drawing.Size(73, 13);
            this.bodyPartFileLocationLabel.TabIndex = 60;
            this.bodyPartFileLocationLabel.Text = "(File Location)";
            // 
            // bodyPartTextureButton
            // 
            this.bodyPartTextureButton.Location = new System.Drawing.Point(12, 216);
            this.bodyPartTextureButton.Name = "bodyPartTextureButton";
            this.bodyPartTextureButton.Size = new System.Drawing.Size(75, 23);
            this.bodyPartTextureButton.TabIndex = 59;
            this.bodyPartTextureButton.Text = "Browse";
            this.bodyPartTextureButton.UseVisualStyleBackColor = true;
            this.bodyPartTextureButton.Click += new System.EventHandler(this.pBrowserButton_Click);
            // 
            // bodyPartHeightLabel
            // 
            this.bodyPartHeightLabel.AutoSize = true;
            this.bodyPartHeightLabel.Location = new System.Drawing.Point(120, 143);
            this.bodyPartHeightLabel.Name = "bodyPartHeightLabel";
            this.bodyPartHeightLabel.Size = new System.Drawing.Size(41, 13);
            this.bodyPartHeightLabel.TabIndex = 58;
            this.bodyPartHeightLabel.Text = "Height:";
            // 
            // bodyPartWidthLabel
            // 
            this.bodyPartWidthLabel.AutoSize = true;
            this.bodyPartWidthLabel.Location = new System.Drawing.Point(22, 143);
            this.bodyPartWidthLabel.Name = "bodyPartWidthLabel";
            this.bodyPartWidthLabel.Size = new System.Drawing.Size(38, 13);
            this.bodyPartWidthLabel.TabIndex = 57;
            this.bodyPartWidthLabel.Text = "Width:";
            // 
            // bodyPartYLabel
            // 
            this.bodyPartYLabel.AutoSize = true;
            this.bodyPartYLabel.Location = new System.Drawing.Point(88, 64);
            this.bodyPartYLabel.Name = "bodyPartYLabel";
            this.bodyPartYLabel.Size = new System.Drawing.Size(17, 13);
            this.bodyPartYLabel.TabIndex = 56;
            this.bodyPartYLabel.Text = "Y:";
            // 
            // bodyPartXLabel
            // 
            this.bodyPartXLabel.AutoSize = true;
            this.bodyPartXLabel.Location = new System.Drawing.Point(19, 64);
            this.bodyPartXLabel.Name = "bodyPartXLabel";
            this.bodyPartXLabel.Size = new System.Drawing.Size(17, 13);
            this.bodyPartXLabel.TabIndex = 55;
            this.bodyPartXLabel.Text = "X:";
            // 
            // bodyPartTextureLabel
            // 
            this.bodyPartTextureLabel.AutoSize = true;
            this.bodyPartTextureLabel.Location = new System.Drawing.Point(9, 189);
            this.bodyPartTextureLabel.Name = "bodyPartTextureLabel";
            this.bodyPartTextureLabel.Size = new System.Drawing.Size(95, 13);
            this.bodyPartTextureLabel.TabIndex = 54;
            this.bodyPartTextureLabel.Text = "Body Part Texture:";
            // 
            // bodyPartSizeLabel
            // 
            this.bodyPartSizeLabel.AutoSize = true;
            this.bodyPartSizeLabel.Location = new System.Drawing.Point(9, 124);
            this.bodyPartSizeLabel.Name = "bodyPartSizeLabel";
            this.bodyPartSizeLabel.Size = new System.Drawing.Size(79, 13);
            this.bodyPartSizeLabel.TabIndex = 53;
            this.bodyPartSizeLabel.Text = "Body Part Size:";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(9, 42);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(0, 13);
            this.label9.TabIndex = 52;
            // 
            // bodyPartLabel
            // 
            this.bodyPartLabel.AutoSize = true;
            this.bodyPartLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bodyPartLabel.Location = new System.Drawing.Point(8, 12);
            this.bodyPartLabel.Name = "bodyPartLabel";
            this.bodyPartLabel.Size = new System.Drawing.Size(86, 20);
            this.bodyPartLabel.TabIndex = 51;
            this.bodyPartLabel.Text = "Body Parts";
            // 
            // externalForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(734, 661);
            this.Controls.Add(this.tabControl);
            this.Name = "externalForm";
            this.Text = "a";
            this.tabPage4.ResumeLayout(false);
            this.tabPage4.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.tabControl.ResumeLayout(false);
            this.tabPage6.ResumeLayout(false);
            this.tabPage6.PerformLayout();
            this.tabPage5.ResumeLayout(false);
            this.tabPage5.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.Button clearMapButton;
        private System.Windows.Forms.Button submitMapButton;
        private System.Windows.Forms.Label mapDesignLabel;
        private System.Windows.Forms.TextBox mapText;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.Label enemyImageLocation;
        private System.Windows.Forms.Button enemyFileClear;
        private System.Windows.Forms.Button enemyFileSubmit;
        private System.Windows.Forms.TextBox enemyFileName;
        private System.Windows.Forms.TextBox enemyAttack;
        private System.Windows.Forms.TextBox enemySpeed;
        private System.Windows.Forms.TextBox enemyHealth;
        private System.Windows.Forms.TextBox enemyHeight;
        private System.Windows.Forms.TextBox enemyWidth;
        private System.Windows.Forms.TextBox enemyY;
        private System.Windows.Forms.TextBox enemyX;
        private System.Windows.Forms.Label enemyFileNameLabel;
        private System.Windows.Forms.RadioButton enemyAggressionFalse;
        private System.Windows.Forms.RadioButton enemyAggressionTrue;
        private System.Windows.Forms.Label enemyAggressionLabel;
        private System.Windows.Forms.Button enemyClear;
        private System.Windows.Forms.Button enemySubmit;
        private System.Windows.Forms.Label enemyAttackLabel;
        private System.Windows.Forms.Label enemySpeedLabel;
        private System.Windows.Forms.Label enemyHealthLabel;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Label enemyHeightLabel;
        private System.Windows.Forms.Label enemyWidthLabel;
        private System.Windows.Forms.Label enemySizeLabel;
        private System.Windows.Forms.Label enemyTextureLabel;
        private System.Windows.Forms.Label enemyLabel;
        private System.Windows.Forms.Label enemyYLabel;
        private System.Windows.Forms.Label enemyXLabel;
        private System.Windows.Forms.Label enemyLocation;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.Label playerImageLocation;
        private System.Windows.Forms.Button playerClear;
        private System.Windows.Forms.Button playerSubmit;
        private System.Windows.Forms.TextBox playerAttack;
        private System.Windows.Forms.TextBox playerSpeed;
        private System.Windows.Forms.TextBox playerHealth;
        private System.Windows.Forms.TextBox playerHeight;
        private System.Windows.Forms.TextBox playerWidth;
        private System.Windows.Forms.TextBox playerY;
        private System.Windows.Forms.TextBox playerX;
        private System.Windows.Forms.Label pAttackLabel;
        private System.Windows.Forms.Label pSpeedLabel;
        private System.Windows.Forms.Label pHealthLabel;
        private System.Windows.Forms.Button pBrowserButton;
        private System.Windows.Forms.Label playerHeightLabel;
        private System.Windows.Forms.Label playerWidthLabel;
        private System.Windows.Forms.Label playerSizeLabel;
        private System.Windows.Forms.Label playerTextureLabel;
        private System.Windows.Forms.Label playerLabel;
        private System.Windows.Forms.Label playerYLocation;
        private System.Windows.Forms.Label playerXLocation;
        private System.Windows.Forms.Label playerLocation;
        private System.Windows.Forms.TabControl tabControl;
        private System.Windows.Forms.TabPage tabPage6;
        private System.Windows.Forms.Label powerUpSizeLabel;
        private System.Windows.Forms.Label powerUpLocationLabel;
        private System.Windows.Forms.Label PowerUpsLabel;
        private System.Windows.Forms.TabPage tabPage5;
        private System.Windows.Forms.Label powerUpTextureLabel;
        private System.Windows.Forms.Label powerUpHeightLabel;
        private System.Windows.Forms.Label powerUpWidthLabel;
        private System.Windows.Forms.Label powerUpYLabel;
        private System.Windows.Forms.Label powerUpXLabel;
        private System.Windows.Forms.TextBox powerUpHeight;
        private System.Windows.Forms.TextBox powerUpWidth;
        private System.Windows.Forms.TextBox powerUpYLocation;
        private System.Windows.Forms.TextBox powerUpXLocation;
        private System.Windows.Forms.Label powerUpFileLabel;
        private System.Windows.Forms.Button powerUpBrowseButton;
        private System.Windows.Forms.Button powerUpClearButton;
        private System.Windows.Forms.Button powerUpSubmitButton;
        private System.Windows.Forms.Button powerUpFileClear;
        private System.Windows.Forms.Button powerUpFileSubmit;
        private System.Windows.Forms.TextBox powerUpFileName;
        private System.Windows.Forms.Label powerUpFileNameLabel;
        private System.Windows.Forms.Button bodyPartFileClearButton;
        private System.Windows.Forms.Button bodyPartFileSubmitButton;
        private System.Windows.Forms.TextBox bodyPartFileName;
        private System.Windows.Forms.Label bodyPartFileNameLabel;
        private System.Windows.Forms.Button bodyPartClearButton;
        private System.Windows.Forms.Button bodyPartSubmitButton;
        private System.Windows.Forms.TextBox bodyPartHeight;
        private System.Windows.Forms.TextBox bodyPartWidth;
        private System.Windows.Forms.TextBox bodyPartY;
        private System.Windows.Forms.TextBox bodyPartX;
        private System.Windows.Forms.Label bodyPartFileLocationLabel;
        private System.Windows.Forms.Button bodyPartTextureButton;
        private System.Windows.Forms.Label bodyPartHeightLabel;
        private System.Windows.Forms.Label bodyPartWidthLabel;
        private System.Windows.Forms.Label bodyPartYLabel;
        private System.Windows.Forms.Label bodyPartXLabel;
        private System.Windows.Forms.Label bodyPartTextureLabel;
        private System.Windows.Forms.Label bodyPartSizeLabel;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label bodyPartLabel;
        private System.Windows.Forms.Label bodyPartLocationLabel;
    }
}

