﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

/*
    External Tool Device
    Purpose: To easily implement
    aspects into the game without
    creating one by one.

    Created by Max De George
    on March 1st, 2017

    **Edited on March 3rd**
    By Max De George

    **Edited on March 6th**
    By Max De George

    **Edited on March 13th**
    By Max De George

    **Edited on March 29th**
    By Max De George

    **Edited on April 7th**
    By Max De George
*/
namespace ExternalToolDevice
{
    public partial class externalForm : Form
    {
        //Attributes
        string playerFile;
        string enemyFile;
        string enemyFileLocation;
        string powerUpFile;
        string powerUpFileLocation;
        string bodyPartFile;
        string bodyPartFileLocation;
        bool powerUpPassed = false;
        bool bodyPartPassed = false;
        Stream stream;
        BinaryWriter infoWrite;
        BinaryReader infoRead;
        StreamReader textReader;
        StreamWriter textWriter;

        public externalForm()
        {
            InitializeComponent();

            if (File.Exists("playerInfo.dat"))
            {
                /*
                MessageBox.Show("playerInfo.dat exists."); //Alerts user if the player file exists. Inconvenient and unnecessary.
                */
                stream = File.OpenRead("playerInfo.dat");
                infoRead = new BinaryReader(stream);

                playerX.Text = "" + infoRead.ReadInt32();
                playerY.Text = "" + infoRead.ReadInt32();
                playerWidth.Text = "" + infoRead.ReadInt32();
                playerHeight.Text = "" + infoRead.ReadInt32();
                playerFile = infoRead.ReadString();
                playerImageLocation.Text = playerFile;
                playerHealth.Text = "" + infoRead.ReadInt32();
                playerSpeed.Text = "" + infoRead.ReadInt32();
                playerAttack.Text = "" + infoRead.ReadInt32();

                infoRead.Close();
            }
            else
            {
                MessageBox.Show("playerInfo.dat does not exist!");
            }

            //Writes the map text file
            if (File.Exists("map.txt"))
            {
                textReader = new StreamReader("map.txt");
                string line = "";
                while ((line = textReader.ReadLine()) != null)
                {
                    mapText.Text += line;
                    mapText.Text += "\n";
                }

                textReader.Close();
            }
        }

        private void tabControl_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        //Prompts user for an image file location
        //Used for file location of images for textures
        private void pBrowserButton_Click(object sender, EventArgs e)
        {
            DialogResult result = openFileDialog1.ShowDialog();
            if (tabControl.SelectedTab == tabPage1)
            {
                playerFile = openFileDialog1.FileName; //Stores the file path into the file string
                playerImageLocation.Text = "(" + playerFile + ")";
            }
            else if (tabControl.SelectedTab == tabPage2)
            {
                enemyFile = openFileDialog1.FileName;
                enemyImageLocation.Text = "(" + enemyFile + ")";
            }
            else if (tabControl.SelectedTab == tabPage6)
            {
                powerUpFile = openFileDialog1.FileName;
                powerUpFileLabel.Text = "(" + powerUpFile + ")";
            }
            else if (tabControl.SelectedTab == tabPage5)
            {
                bodyPartFile = openFileDialog1.FileName;
                bodyPartFileLocationLabel.Text = "(" + bodyPartFile + ")";
            }
        }

        //Clears input fields for the player tab
        private void playerClear_Click(object sender, EventArgs e)
        {
            playerX.Clear();
            playerY.Clear();
            playerWidth.Clear();
            playerHeight.Clear();
            playerFile = "";
            playerImageLocation.Text = "(File Location)";
            playerHealth.Clear();
            playerSpeed.Clear();
            playerAttack.Clear();
        }

        //Clears the input fields for the enemy tab
        private void enemyClear_Click(object sender, EventArgs e)
        {
            enemyX.Clear();
            enemyY.Clear();
            enemyWidth.Clear();
            enemyHeight.Clear();
            enemyFile = "";
            enemyImageLocation.Text = "(File Location)";
            enemyHealth.Clear();
            enemySpeed.Clear();
            enemyAttack.Clear();
            enemyAggressionFalse.Checked = false;
            enemyAggressionTrue.Checked = true;
        }

        //File submission for enemy. Determines what to name the file.
        private void enemyFileSubmit_Click(object sender, EventArgs e)
        {
            string file = enemyFileName.Text;
            enemyFileLocation = file;

            if (File.Exists(file))
            {
                MessageBox.Show("File exists");

                stream = File.OpenRead(enemyFileLocation);
                infoRead = new BinaryReader(stream);

                enemyX.Text = "" + infoRead.ReadInt32();
                enemyY.Text = "" + infoRead.ReadInt32();
                enemyWidth.Text = "" + infoRead.ReadInt32();
                enemyHeight.Text = "" + infoRead.ReadInt32();
                enemyFile = infoRead.ReadString();
                enemyImageLocation.Text = enemyFile;
                enemyHealth.Text = "" + infoRead.ReadInt32();
                enemySpeed.Text = "" + infoRead.ReadInt32();
                enemyAttack.Text = "" + infoRead.ReadInt32();

                if (infoRead.ReadBoolean() == true)
                {
                    enemyAggressionTrue.Checked = true;
                    enemyAggressionFalse.Checked = false;
                }
                else
                {
                    enemyAggressionTrue.Checked = false;
                    enemyAggressionFalse.Checked = true;
                }

                infoRead.Close();
            }
            else
            {
                MessageBox.Show("File doesn't exist.\nCreating File.");
                enemyFileLocation = file;

                enemyX.Clear();
                enemyY.Clear();
                enemyWidth.Clear();
                enemyHeight.Clear();
                playerFile = "";
                playerImageLocation.Text = "(File Location)";
                enemyHealth.Clear();
                enemySpeed.Clear();
                enemyAttack.Clear();
                enemyAggressionFalse.Checked = false;
                enemyAggressionTrue.Checked = true;

            }
        }

        //Submission for the player tab. Adds content to the player file.
        private void playerSubmitButton_Click(object sender, EventArgs e)
        {
            if (!File.Exists("playerInfo.dat"))
            {
                try
                {
                    stream = File.OpenWrite("playerInfo.dat");
                    infoWrite = new BinaryWriter(stream);

                    infoWrite.Write(int.Parse(playerX.Text));
                    infoWrite.Write(int.Parse(playerY.Text));
                    infoWrite.Write(int.Parse(playerWidth.Text));
                    infoWrite.Write(int.Parse(playerHeight.Text));
                    infoWrite.Write(playerFile);
                    infoWrite.Write(int.Parse(playerHealth.Text));
                    infoWrite.Write(int.Parse(playerSpeed.Text));
                    infoWrite.Write(int.Parse(playerAttack.Text));

                    infoWrite.Close();

                    MessageBox.Show("Info saved to playerInfo.dat");
                }
                catch (Exception except)
                {
                    except = null;
                }
            }
            else
            {
                try
                {
                    stream = File.OpenWrite("playerInfo.dat");
                    infoWrite = new BinaryWriter(stream);

                    infoWrite.Write(int.Parse(playerX.Text));
                    infoWrite.Write(int.Parse(playerY.Text));
                    infoWrite.Write(int.Parse(playerWidth.Text));
                    infoWrite.Write(int.Parse(playerHeight.Text));
                    infoWrite.Write(playerFile);
                    infoWrite.Write(int.Parse(playerHealth.Text));
                    infoWrite.Write(int.Parse(playerSpeed.Text));
                    infoWrite.Write(int.Parse(playerAttack.Text));

                    infoWrite.Close();

                    MessageBox.Show("Info saved to playerInfo.dat");
                }
                catch (Exception except)
                {

                }
            }
        }

        //Submission for the enemy tab. Saves the information to a file designated in form.
        private void enemySubmitButton_Click(object sender, EventArgs e)
        {
            bool validInput = true;
            int holdValue = 0;

            if (int.TryParse(enemyX.Text, out holdValue) && int.TryParse(enemyY.Text, out holdValue) && int.TryParse(enemyWidth.Text, out holdValue)
                && int.TryParse(enemyHeight.Text, out holdValue) && enemyFile != "" && int.TryParse(enemyHealth.Text, out holdValue)
                && int.TryParse(enemySpeed.Text, out holdValue) && int.TryParse(enemyAttack.Text, out holdValue))
            {
                validInput = true;
            }
            else
            {
                validInput = false;
            }


            if ((File.Exists(enemyFileName.Text) && enemyFileName.Text.Substring(enemyFileLocation.Length - 4, 4) == ".dat") && validInput == true)
            {
                stream = File.OpenWrite(enemyFileLocation);
                infoWrite = new BinaryWriter(stream);

                infoWrite.Write(int.Parse(enemyX.Text));
                infoWrite.Write(int.Parse(enemyY.Text));
                infoWrite.Write(int.Parse(enemyWidth.Text));
                infoWrite.Write(int.Parse(enemyHeight.Text));
                infoWrite.Write(enemyFile);
                infoWrite.Write(int.Parse(enemyHealth.Text));
                infoWrite.Write(int.Parse(enemySpeed.Text));
                infoWrite.Write(int.Parse(enemyAttack.Text));

                if (enemyAggressionTrue.Checked == true)
                {
                    infoWrite.Write(true);
                }
                else
                {
                    infoWrite.Write(false);
                }

                infoWrite.Close();
            }
            else
            {
                MessageBox.Show("File name cannot be used.");
            }

        }

        //Clears the enemy file field.
        private void enemyFileClear_Click(object sender, EventArgs e)
        {
            enemyFileName.Clear();
        }

        //Submits map as a .txt file.
        private void submitMapButton_Click(object sender, EventArgs e)
        {
            if(File.Exists("map.txt"))
            {
                DialogResult dialogChoice = MessageBox.Show("Are you sure you want to update the map?", "Warning", MessageBoxButtons.YesNo);

                string[] stringArray = mapText.Lines;
                string errorMessage = "";

                for (int x = 0; x < stringArray.Length; x++)
                {
                    if(stringArray[x].Length < 1000)
                    {
                        errorMessage += "\nThere is an error on line " + (x + 1) + ". The string is " + (1000 - stringArray[x].Length) + " too short/long.";
                    }
                }

                if(576 - stringArray.Length != 0)
                {
                    errorMessage += "\nThere is an error. The text document is not long enough. You are missing " + (576 - stringArray.Length) + " lines of values.";
                }


                if (dialogChoice == DialogResult.Yes && errorMessage == "")
                {
                    try
                    {
                        File.WriteAllText("map.txt", String.Empty);

                        string[] textLines;

                        textWriter = new StreamWriter("map.txt");

                        textLines = mapText.Text.Split('\n'); //Splits the text into lines of text

                        foreach (string textLine in textLines)
                        {
                            textWriter.WriteLine(textLine);
                        }

                        textWriter.Close();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("Sorry, there seems that have been an error.\nError: " + ex.Message);
                    }
                }
                else if(dialogChoice == DialogResult.No)
                {
                    
                }
                else if(errorMessage != "")
                {
                    MessageBox.Show("There was an error trying to save the map.\n" + errorMessage);
                    errorMessage = "";
                }
            }
            else
            {
                try
                {
                    string[] textLines;

                    textWriter = new StreamWriter("map.txt");

                    textLines = mapText.Text.Split('\n'); //Splits the text into lines of text

                    foreach (string textLine in textLines)
                    {
                        textWriter.WriteLine(textLine);
                    }

                    textWriter.Close();
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Sorry, there seems that have been an error.\nError: " + ex.Message);
                }
            }
        }

        //Clears the map design
        private void clearMapbutton_Click(object sender, EventArgs e)
        {
            mapText.Clear();
        }

        //Submits the power info to designated file.
        private void powerUpSubmitButton_Click(object sender, EventArgs e)
        {
            bool validInput = true;
            int holdValue = 0;

            if (int.TryParse(powerUpXLocation.Text, out holdValue) && int.TryParse(powerUpYLocation.Text, out holdValue) && int.TryParse(powerUpWidth.Text, out holdValue)
                && int.TryParse(powerUpHeight.Text, out holdValue) && powerUpFile != "")
            {
                validInput = true;
            }
            else
            {
                validInput = false;
            }

            if ((File.Exists(powerUpFileName.Text) || powerUpPassed) && validInput)
            {
                stream = File.OpenWrite(powerUpFileLocation);
                infoWrite = new BinaryWriter(stream);

                infoWrite.Write(int.Parse(powerUpXLocation.Text));
                infoWrite.Write(int.Parse(powerUpYLocation.Text));
                infoWrite.Write(int.Parse(powerUpWidth.Text));
                infoWrite.Write(int.Parse(powerUpHeight.Text));
                infoWrite.Write(powerUpFile);
                infoWrite.Close();
            }
            else
            {
                MessageBox.Show("File cannot be used.");
            }
        }

        //Clears the input fields for the powerUp tab
        private void powerUpClearButton_Click(object sender, EventArgs e)
        {
            powerUpXLocation.Clear();
            powerUpYLocation.Clear();
            powerUpWidth.Clear();
            powerUpHeight.Clear();
            powerUpFile = "";
            powerUpFileLabel.Text = "(File Location)";
        }

        //Determines if file name is a valid name and can be used.
        private void powerUpFileSubmit_Click(object sender, EventArgs e)
        {
            if(powerUpFileName.Text == "" || powerUpFileName.Text.Length < 5 || powerUpFileName.Text.Substring(powerUpFileName.Text.Length - 4, 4) != ".dat")
            {
                MessageBox.Show("Invalid file type. Must be a .dat file and must have a name.");
                powerUpPassed = false;
            }
            else if(!File.Exists(powerUpFileName.Text) && powerUpFileName.Text.Substring(powerUpFileName.Text.Length - 4, 4) == ".dat")
            {
                MessageBox.Show("File doesn't exist.\nCreating File.");
                powerUpFileLocation = powerUpFileName.Text;
                powerUpPassed = true;
            }
            else if(File.Exists(powerUpFileName.Text))
            {
                powerUpFileLocation = powerUpFileName.Text;
                stream = File.OpenRead(powerUpFileLocation);
                infoRead = new BinaryReader(stream);
                powerUpXLocation.Text = "" + infoRead.ReadInt32();
                powerUpYLocation.Text = "" + infoRead.ReadInt32();
                powerUpWidth.Text = "" + infoRead.ReadInt32();
                powerUpHeight.Text = "" + infoRead.ReadInt32();
                powerUpFile = infoRead.ReadString();
                powerUpFileLabel.Text = powerUpFile;
                powerUpPassed = true;
            }
        }

        //Clears the file name in power up tab
        private void powerUpFileClear_Click(object sender, EventArgs e)
        {
            powerUpFileName.Text = "";
        }

        //Submits the entire form for the body part tab
        private void bodyPartSubmitButton_Click(object sender, EventArgs e)
        {
            bool validInput = true;
            int holdValue = 0;

            if(int.TryParse(bodyPartX.Text, out holdValue) && int.TryParse(bodyPartY.Text, out holdValue) && int.TryParse(bodyPartWidth.Text, out holdValue)
                && int.TryParse(bodyPartHeight.Text, out holdValue) && bodyPartFile != "")
            {
                validInput = true;
            }
            else
            {
                validInput = false;
            }

            if ((File.Exists(bodyPartFileName.Text) || bodyPartPassed) && validInput == true)
            {
                stream = File.OpenWrite(bodyPartFileLocation);
                infoWrite = new BinaryWriter(stream);

                infoWrite.Write(int.Parse(bodyPartX.Text));
                infoWrite.Write(int.Parse(bodyPartY.Text));
                infoWrite.Write(int.Parse(bodyPartWidth.Text));
                infoWrite.Write(int.Parse(bodyPartHeight.Text));
                infoWrite.Write(bodyPartFile);
                infoWrite.Close();
            }
            else
            {
                MessageBox.Show("File cannot be used.");
            }
        }

        //Clears the entire body part tab input fields
        private void bodyPartClearButton_Click(object sender, EventArgs e)
        {
            bodyPartX.Clear();
            bodyPartY.Clear();
            bodyPartWidth.Clear();
            bodyPartHeight.Clear();
            bodyPartFile = "";
            bodyPartFileLocationLabel.Text = "(File Location)";
        }

        //Sumbits a file name, checks to see if it is a valid name, and uses it
        private void bodyPartFileSubmitButton_Click(object sender, EventArgs e)
        {
            if (bodyPartFileName.Text == "" || bodyPartFileName.Text.Length < 5 || bodyPartFileName.Text.Substring(bodyPartFileName.Text.Length - 4, 4) != ".dat")
            {
                MessageBox.Show("Invalid file type. Must be a .dat file and must have a name.");
                bodyPartPassed = false;
            }
            else if (!File.Exists(bodyPartFileName.Text) && bodyPartFileName.Text.Substring(bodyPartFileName.Text.Length - 4, 4) == ".dat")
            {
                MessageBox.Show("File doesn't exist.\nCreating File.");
                bodyPartFileLocation = bodyPartFileName.Text;
                bodyPartPassed = true;
            }
            else if (File.Exists(bodyPartFileName.Text))
            {
                bodyPartFileLocation = bodyPartFileName.Text;
                stream = File.OpenRead(bodyPartFileLocation);
                infoRead = new BinaryReader(stream);
                bodyPartX.Text = "" + infoRead.ReadInt32();
                bodyPartY.Text = "" + infoRead.ReadInt32();
                bodyPartWidth.Text = "" + infoRead.ReadInt32();
                bodyPartHeight.Text = "" + infoRead.ReadInt32();
                bodyPartFile = infoRead.ReadString();
                bodyPartFileLocationLabel.Text = powerUpFile;
                bodyPartPassed = true;
            }
        }

        //Clears the file name in the input field on the body part tab.
        private void bodyPartFileClearButton_Click(object sender, EventArgs e)
        {
            bodyPartFileName.Text = "";
        }
    }
}
